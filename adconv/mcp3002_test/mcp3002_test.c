/*
 * MCP3002(2ch A/D Converter)のテストプログラム
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "spi_api.h"

#define SPI_SPEED (75 * 1000)

/*----------------------------------------------------------------------*/

static struct spi_config_info config_info = {
    .mode = SPI_MODE_0,
    .speed = SPI_SPEED,
    .flags = 0,
    .dev = "/dev/spidev0.0",
    .chip_sel = 0,
};

static struct spi_handle *spih;

void mcp3002_init(void)
{
    spih = spi_alloc_bus(&config_info);
    if(spih == NULL) {
	fprintf(stderr, "Error: spi_alloc_bus()\n");
	exit(1);
    }
}

int mcp3002_get(unsigned ch)
{
    unsigned char in[2], out[2];

    out[0] = 0x60 | (ch << 4);
    spih->tx_buf = (uintptr_t)out;
    spih->rx_buf = (uintptr_t)in;
    spih->len = sizeof(out);

    int ret = spi_xfer(spih);
    if(ret < 0) {
	fprintf(stderr, "Error: SPI_IOC_MESSAGE failed\n");
	return -1;
    }

    return ((in[0] << 8) | in[1]) & 0x3ff;
}

/*----------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    mcp3002_init();

    for(;;) {
	int v0, v1;
	v0 = mcp3002_get(0);
	v1 = mcp3002_get(1);

	printf("ch0=%4d ch1=%4d\r", v0, v1);
    }

    return 0;
}
