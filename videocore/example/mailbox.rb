require '../videocore/mailbox'

if __FILE__ == $0
    def to_h(array, sep=' ')
	return array.map{|i| '0x' + i.to_s(16)}.join(sep)
    end
    
    def to_d(array, sep=' ')
	return array.map{|i| i.to_s}.join(sep)
    end
    
    mb = Mailbox.new
    # p mb.raw_call(0x00010001, [0])
    # p mb.raw_call(0x00030011, [0,0,0,0])
    # p mb.methods

    puts "get_firmware_revision: #{to_h(mb.get_firmware_revision)}"
    puts "get_board_model: #{to_h(mb.get_board_model)}"
    puts "get_board_revision: #{to_h(mb.get_board_revision)}"
    puts "get_mac_address: #{mb.get_mac_address.map{|x| x.to_s(16)}.join(':')}"
    puts "get_board_serial: #{to_h(mb.get_board_serial)}"
    puts "get_arm_memory: #{to_h(mb.get_arm_memory)}"
    puts "get_vc_memory:  #{to_h(mb.get_vc_memory)}"

    Mailbox::POWER_DEVICE_ID.length.times do |id|
	name = Mailbox::POWER_DEVICE_ID[id]
	puts "Device: #{name}"
	puts " get_power_state: #{to_d(mb.get_power_state(id))}"
	puts " get_timing:      #{to_d(mb.get_timing(id))}"
    end
    Mailbox::CLOCK_ID.length.times do |id|
	name = Mailbox::CLOCK_ID[id]
	puts "Clock: #{name}"
	puts " get_clock_state:    #{to_d(mb.get_clock_state(id))}"
	puts " get_clock_rate:     #{to_d(mb.get_clock_rate(id))}"
	puts " get_max_clock_rate: #{to_d(mb.get_max_clock_rate(id))}"
	puts " get_min_clock_rate: #{to_d(mb.get_min_clock_rate(id))}"
    end
    puts "get_turbo: #{to_d(mb.get_turbo(0))}"
    Mailbox::VOLTAGE_ID.length.times do |id|
	name = Mailbox::VOLTAGE_ID[id]
	puts "Voltage: #{name}"
	puts " get_voltage:     #{to_d(mb.get_voltage(id))}"
	puts " get_max_voltage: #{to_d(mb.get_max_voltage(id))}"
	puts " get_min_voltage: #{to_d(mb.get_min_voltage(id))}"
    end
    puts "get_temperature:     #{to_d(mb.get_temperature(0))}"
    puts "get_max_temperature: #{to_d(mb.get_max_temperature(0))}"

    edid = mb.get_edid_block
    puts "get_edid_block: #{edid.length} bytes"
    edid.each_with_index do |t, i|
	printf "%02x ", t
	puts if i % 16 == 15
    end

    puts "get_physical_display_size: #{to_d(mb.get_physical_display_size)}"
    puts "get_virtual_buffer_size:   #{to_d(mb.get_virtual_buffer_size)}"
    puts "get_depth: #{to_d(mb.get_depth)}"
    puts "get_pixel_order: #{to_d(mb.get_pixel_order)}"
    puts "get_alpha_mode: #{to_d(mb.get_alpha_mode)}"
    puts "get_pitch: #{to_d(mb.get_pitch)}"
    puts "get_virtual_offset: #{to_d(mb.get_virtual_offset)}"
    puts "get_overscan: #{to_d(mb.get_overscan)}"
end
