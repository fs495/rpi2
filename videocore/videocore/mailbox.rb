# coding: utf-8
# 
# VideoCore Mailbox interface (experimental)
#
# reference:
# https://github.com/nineties/py-videocore/tree/master/videocore
# https://github.com/raspberrypi/firmware/wiki/MailBox-property-interface

class Mailbox
    IOCTL_MAILBOX = 0xC0046400   # _IOWR(100, 0, char *)

    # request/response code
    PROCESS_REQUEST = 0x00000000
    REQUEST_SUCCESS = 0x80000000
    PARSE_ERROR     = 0x80000001
    END_TAG	    = 0
    
    METHODS = [
	# name				tag_id	 	req	res
	['get_firmware_revision',	0x00000001,	0,	1],
	['get_board_model',		0x00010001,	0,	1],
	['get_board_revision',		0x00010002,	0,	1],
	['_get_mac_address',		0x00010003,	0,	(6+2)/4],
	['_get_board_serial',		0x00010004,	0,	2],
	['get_arm_memory',		0x00010005,	0,	2],
	['get_vc_memory',		0x00010006,	0,	2],
	#['get_clocks',			0x00010007,	0,	],
	['get_power_state',		0x00020001,	1,	2],
	['get_timing',			0x00020002,	1,	2],
	['set_power_state',		0x00028001,	2,	2],
	['get_clock_state',		0x00030001,	1,	2],
	['set_clock_state',		0x00038001,	2,	2],
	['get_clock_rate',		0x00030002,	1,	2],
	['set_clock_rate',		0x00038002,	3,	2],
	['get_max_clock_rate',		0x00030004,	1,	2],
	['get_min_clock_rate',		0x00030007,	1,	2],
	['get_turbo',			0x00030009,	1,	2],
	['set_turbo',			0x00038009,	2,	2],
	['get_voltage',			0x00030003,	1,	2],
	['set_voltage',			0x00038003,	2,	2],
	['get_max_voltage',		0x00030005,	1,	2],
	['get_min_voltage',		0x00030008,	1,	2],
	['get_temperature',		0x00030006,	1,	2],
	['get_max_temperature',		0x0003000a,	1,	2],
	['allocate_memory',		0x0003000c,	3,	1],
	['lock_memory',			0x0003000d,	1,	1],
	['unlock_memory',		0x0003000e,	1,	1],
	['release_memory',		0x0003000f,	1,	1],
	['execute_code',		0x00030010,	7,	1],
	['execute_qpu',			0x00030011,	4,	1],
	['enable_qpu',			0x00030012,	1,	1],
	['get_dispmax_resource_mem_handle',	0x00030014,	1,	2],
	['_get_edid_block',		0x00030020,	1,	2+128/4],
	['allocate_buffer',		0x00040001,	1,	2],
	['release_buffer',		0x00048001,	0,	0],
	['blank_screen',		0x00040002,	1,	1],
	['get_physical_display_size',	0x00040003,	0,	2],
	['test_physical_display_size',	0x00044003,	2,	2],
	['set_physical_display_size',	0x00048003,	2,	2],
	['get_virtual_buffer_size',	0x00040004,	0,	2],
	['test_virtual_buffer_size',	0x00044004,	2,	2],
	['set_virtual_buffer_size',	0x00048004,	2,	2],
	['get_depth',			0x00040005,	0,	1],
	['test_depth',			0x00044005,	1,	1],
	['set_depth',			0x00048005,	1,	1],
	['get_pixel_order',		0x00040006,	0,	1],
	['test_pixel_order',		0x00044006,	1,	1],
	['set_pixel_order',		0x00048006,	1,	1],
	['get_alpha_mode',		0x00040007,	0,	1],
	['test_alpha_mode',		0x00044007,	1,	1],
	['set_alpha_mode',		0x00048007,	1,	1],
	['get_pitch',			0x00040008,	0,	1],
	['get_virtual_offset',		0x00040009,	0,	2],
	['test_virtual_offset',		0x00044009,	2,	2],
	['set_virtual_offset',		0x00048009,	2,	2],
	['get_overscan',		0x0004000a,	0,	4],
	['test_overscan',		0x0004400a,	4,	4],
	['set_overscan',		0x0004800a,	4,	4],
	#['get_palette',		0x0004000b,	0,	1024/4],
	#['test_palette',		0x0004400b,	nil,	nil],
	#['set_palette',		0x0004800b,	nil,	nil],
	#['get_command_line',		0x00050001,]
	['get_dma_channels',		0x00060001,	0,	1],
	['set_cursor_state',		0x00008010,	4,	1],
	['set_cursor_info',		0x00008011,	6,	1],
    ]

    POWER_DEVICE_ID = [
	'SD Card', 'UART0', 'UART1', 'USB HCD', 'I2C0', 'I2C1', 'SPI', 'CCP2TX'
    ]

    CLOCK_ID = [
	'reserved', 'EMMC', 'UART', 'ARM',
	'CORE', 'V3D', 'H264', 'ISP',
	'SDRAM', 'PIXEL', 'PWM'
    ]

    VOLTAGE_ID = [
	'reserved', 'Core', 'SDRAM_C', 'SDRAM_P', 'SDRAM_I'
    ]

    def initialize
	# メモ: ioctl(TCGETS)を発行するのでカーネルメッセージに警告が出る
	@fd = open("/dev/vcio")
    end

    def raw_call(tag_id, values)
	req_data = [
		 (5 + values.length + 1) * 4,# buffer size in bytes
		 PROCESS_REQUEST, # buffer request/response code 
		 tag_id,
		 values.length * 4, # value buffer size in bytes
		 0, # request/response code
		 values,
		 END_TAG
		].flatten
	# puts "request: " + req_data.map{|x| x.to_s(16)}.join(',')
	data = req_data.pack('L*')
	@fd.ioctl(IOCTL_MAILBOX, data)
	res_data = data.unpack('L*')
	# puts "reply: " + res_data.map{|x| x.to_s(16)}.join(',')
	return res_data[5, res_data.length]
    end

    METHODS.each do |name, tag_id, req, res|
	define_method(name) do |*args|
	    if(args.length < req)
		raise "Too few arguments for #{name} (#{args.length} for #{req})"
	    end
	    if(args.length < res)
		args = args + Array.new(res - args.length, 0)
	    end
	    ret = raw_call(tag_id, args)
	    return ret[0, res]
	end
    end

    def get_mac_address()
	a = _get_mac_address()
	return a.pack('L*').unpack('C6x2')
    end

    def get_board_serial()
	a = _get_board_serial()
	return a.pack('L*').unpack('Q')
    end

    def get_edid_block()
	block = 0
	ret = []
	while true
	    tmp = _get_edid_block(block)
	    break if tmp[1] != 0

	    ret = ret + tmp[2, tmp.length].pack('L*').unpack('C*')
	    block += 1
	end
	return ret
    end
end

