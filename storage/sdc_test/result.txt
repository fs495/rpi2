-*- outline -*-

* ######################################################################
CMD0+CMD8

Transcend SDHCでは電源オン後、最初だけ以下のような応答に…
GO_IDLE_STATE(CMD0)
  sdc_cmd_token: CMD0 0x00000000 0x95
  sdc_wait_r1: ff 01
SEND_IF_COND(CMD8)
  sdc_cmd_token: CMD8 0x000001aa 0x87
  sdc_wait_r1: ff c1 7f

2回め以降:
Entering SPI mode
GO_IDLE_STATE(CMD0)
  sdc_cmd_token: CMD0 0x00000000 0x95
  sdc_wait_r1: ff 01
SEND_IF_COND(CMD8)
  sdc_cmd_token: CMD8 0x000001aa 0x87
  sdc_wait_r1: ff 01
  CMD8 resp = 000001aa


* ######################################################################
CSD

** Canon 16MB SDSC
SEND_CSD(CMD9)
  sdc_cmd_token: CMD9 0x00000000 0x00
  sdc_wait_r1: ff ff 00
  sdc_trasaction_read: fe
       +0 +1 +2 +3 +4 +5 +6 +7 +8 +9 +A +B +C +D +E +F 
  0000 00 5d 01 32 13 59 80 e3 76 d9 cf ff 16 40 00 4f .].2.Y..v....@.O

** Transcend SDHC
END_CSD(CMD9)
  sdc_cmd_token: CMD9 0x00000000 0x00
  sdc_wait_r1: ff 00
  sdc_trasaction_read: fe
       +0 +1 +2 +3 +4 +5 +6 +7 +8 +9 +A +B +C +D +E +F 
  0000 40 0e 00 32 5b 59 00 00 77 dd 7f 80 0a 40 00 41 @..2[Y..w....@.A

* ######################################################################
CID

** Canon 16MB SDSC
SEND_CID(CMD10)
  sdc_cmd_token: CMD10 0x00000000 0x00
  sdc_wait_r1: ff ff 00
  sdc_trasaction_read: fe
       +0 +1 +2 +3 +4 +5 +6 +7 +8 +9 +A +B +C +D +E +F 
  0000 01 50 41 53 30 31 36 42 41 46 2d 2f ae 00 46 55 .PAS016BAF-/..FU
       MIDOID   PNM            PRVPSN         MDT   CRC

** Transcend SDHC
SEND_CID(CMD10)
  sdc_cmd_token: CMD10 0x00000000 0x00
  sdc_wait_r1: ff 00
  sdc_trasaction_read: fe
       +0 +1 +2 +3 +4 +5 +6 +7 +8 +9 +A +B +C +D +E +F 
  0000 74 4a 60 53 44 55 31 20 10 4b c4 cb df 00 fb ed tJ`SDU1 .K......
