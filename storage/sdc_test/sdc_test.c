#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "gpio/uspace_gpio.h"

enum sdc_gpio_config {
    GPIO_CE = 8,
    GPIO_MISO = 9,
    GPIO_MOSI = 10,
    GPIO_SCLK = 11,
};

enum {
    SDC_MAX_INIT_RETRIES = 8,
    SDC_MAX_RETRIES = 100,
    SDC_SLOW_CLK = 10000,  /* 100kHz => 10us */
    SDC_FAST_CLK = 40,  /* 25MHz => 40ns */
};

enum sdc_type {
    SDC_UNKNOWN,
    SDC_V1,
    SDC_V2_BLOCK,
    SDC_V2_BYTE,
    MMC_V3,
};

static struct uspace_iop_handle *ioph;
static struct timespec hperiod;
static uint8_t sdctype;

/*----------------------------------------------------------------------*/

void io_init(void)
{
    ioph = gpio_alloc_uspace();
    gpio_select_func(ioph, GPIO_CE, GPFSEL_OUTPUT);
    gpio_select_func(ioph, GPIO_MISO, GPFSEL_INPUT);
    gpio_select_func(ioph, GPIO_MOSI, GPFSEL_OUTPUT);
    gpio_select_func(ioph, GPIO_SCLK, GPFSEL_OUTPUT);
}

unsigned io_xfer(unsigned out)
{
    int bit;
    unsigned in = 0;

    for(bit = 7; bit >= 0; bit--) {
	gpio_clr(ioph, GPIO_SCLK);
	if(out & (1 << bit))
	    gpio_set(ioph, GPIO_MOSI);
	else
	    gpio_clr(ioph, GPIO_MOSI);
	nanosleep(&hperiod, NULL);

	gpio_set(ioph, GPIO_SCLK);
	in = (in << 1) | gpio_get(ioph, GPIO_MISO);
	nanosleep(&hperiod, NULL);
    }

    return in;
}

/*----------------------------------------------------------------------*/

void sdc_start_transaction(void)
{
    gpio_clr(ioph, GPIO_CE);
    nanosleep(&hperiod, NULL);
}

void sdc_finish_transaction(void)
{
    gpio_set(ioph, GPIO_CE);
    nanosleep(&hperiod, NULL);
    io_xfer(0xff);
}

void sdc_cmd_token(uint8_t cmd, uint32_t arg, uint8_t crc)
{
    printf("  sdc_cmd_token: CMD%d %08x %02x\n", cmd, arg, crc);
    io_xfer(cmd | 0x40);
    io_xfer(arg >> 24);
    io_xfer(arg >> 16);
    io_xfer(arg >> 8);
    io_xfer(arg);
    io_xfer(crc);
}

uint8_t sdc_wait_cmd_resp(void)
{
    unsigned in, retries;

    printf("  sdc_wait_cmd_resp:");
    for(retries = 0; retries < SDC_MAX_RETRIES; retries++) {
	in = io_xfer(0xff);
	printf(" %02x", in);
	if((in & 0x80) == 0)
	    break;
	nanosleep(&hperiod, NULL);
    }
    printf("\n");
    if(retries == SDC_MAX_RETRIES) {
	fprintf(stderr, "Error: no reponse\n");
	return -1;
    }
    return in;
}

uint32_t sdc_recv_4octets(void)
{
    unsigned in = io_xfer(0xff);
    in = (in << 8) | io_xfer(0xff);
    in = (in << 8) | io_xfer(0xff);
    in = (in << 8) | io_xfer(0xff);
    return in;
}

void sdc_send_acmd(uint8_t cmd, uint32_t arg, uint8_t crc)
{
    sdc_cmd_token(55, 0, 0);
    sdc_wait_cmd_resp();
    sdc_finish_transaction();

    sdc_start_transaction();
    sdc_cmd_token(cmd, arg, crc);
}

unsigned sdc_read_data(int len, char *buf)
{
    printf("  sdc_trasaction_read:");
    // Start Block Tokenを待つ
    int retries;
    for(retries = 0; retries < SDC_MAX_RETRIES; retries++) {
	uint8_t r = io_xfer(0xff);
	printf(" %02x", r);
	if((r & 0xf0) == 0x00) {
	    // Data Error Token
	    return r & 0x0f;
	} else if(r == 0xfe) {
	    // Start Block Token
	    break;
	}
    }
    if(retries == SDC_MAX_RETRIES) {
	printf(" gave up\n");
	return -1;
    }
    printf("\n");

    for(int i = 0; i < len; i++)
	buf[i] = io_xfer(0xff);
    uint16_t crc = io_xfer(0xff);
    crc = (crc << 8) | io_xfer(0xff);
    return 0;
}

unsigned sdc_write_data(int len, char *buf)
{
    int i;
    uint8_t r1;

    printf("  sdc_write_data: (data_resp)");
    io_xfer(0xff); // Dummy
    io_xfer(0xfe); // Start Block Token
    for(i = 0; i < len; i++)
	io_xfer(buf[i]);
    io_xfer(0xff); // CRC
    io_xfer(0xff); // CRC

    for(i = 0; i < SDC_MAX_RETRIES; i++) {
	r1 = io_xfer(0xff);
	printf(" %02x", r1);
	if((r1 & 0x11) == 0x01) // Data Resp Token
	    break;
    }
    if((r1 & 0x1f) != 0x05 || i == SDC_MAX_RETRIES) {
	printf(" gave up");
	return r1;
    }
    printf(" (busy)");
    for(i = 0; i < SDC_MAX_RETRIES; i++) {
	r1 = io_xfer(0xff);
	printf(" %02x", r1);
	if(r1 == 0xff)
	    break;
    }
    printf("\n");
    return (r1 == 0xff) ? 0 : - 1;
}


/*----------------------------------------------------------------------*/

static void sdc_init_spi_mode()
{
    printf("Entering SPI mode\n");
    gpio_set(ioph, GPIO_CE);
    gpio_set(ioph, GPIO_MOSI);
    nanosleep(&hperiod, NULL);
    for(int i = 0; i < 16; i++)
	io_xfer(0xff);
}

static uint8_t sdc_identify(void)
{
    unsigned r1, retries, resp;

    /*
     * CMD0とCMD8を送信する。
     * R1 responseが0x01(SD v2で返ってくるはずの値)でなければリトライする
     */
    for(retries = 0; retries < SDC_MAX_INIT_RETRIES; retries++) {
	printf("GO_IDLE_STATE(CMD0)\n");
	sdc_start_transaction();
	sdc_cmd_token(0, 0, 0x95);
	r1 = sdc_wait_cmd_resp();
	sdc_finish_transaction();
	if(r1 != 0x01)
	    continue;

	printf("SEND_IF_COND(CMD8)\n");
	sdc_start_transaction();
	sdc_cmd_token(8, 0x000001aa, 0x87);
	r1 = sdc_wait_cmd_resp();
	resp = sdc_recv_4octets();
	sdc_finish_transaction();
	if(r1 != 0x01)
	    continue;
	break;
    }

    /*
     * CMD8が受け付けたどうかでSD v2以降か以前か判断し、初期設定する
     */
    if(r1 == 0x01) {
	// CMD8を正常に受け付けたのでSD v2以降
	printf("  CMD8 resp = %08x\n", resp);
	if((resp & 0xfff) != 0x1aa) {
	    printf("Error: unknown card\n");
	    return SDC_UNKNOWN;
	}

	for(;;) {
	    printf("SD_SEND_OP_COND(ACMD41)\n");
	    sdc_start_transaction();
	    sdc_send_acmd(41, 0x40000000, 0);
	    r1 = sdc_wait_cmd_resp();
	    sdc_finish_transaction();

	    if(r1 == 0) {
		printf("Identified: SD v2\nREAD_OCR(CMD58)\n");
		sdc_start_transaction();
		sdc_cmd_token(58, 0, 0);
		r1 = sdc_wait_cmd_resp();
		resp = sdc_recv_4octets();
		sdc_finish_transaction();
		printf("  OCR = %08x, addr = %s\n", resp,
		       (resp & (1<<30) ? "block" : "byte"));
		return resp & (1 << 30) ? SDC_V2_BLOCK : SDC_V2_BYTE;
	    } else if(r1 != 1) {
		printf("Error: unknown card\n");
		return SDC_UNKNOWN;
	    }
	}
    } else if(r1 == 0x05) {
	// Illegal Commandを示す有効なR1 Responseならば、MMCかSD_v1
	for(;;) {
	    printf("SD_SEND_OP_COND(ACMD41)\n");
	    sdc_start_transaction();
	    sdc_send_acmd(41, 0, 0);
	    r1 = sdc_wait_cmd_resp();
	    sdc_finish_transaction();

	    if(r1 == 0) {
		printf("Identified: SD v1\n");
		return SDC_V1;
	    } else if(r1 != 1) {
		break;
	    }
	}

	for(;;) {
	    printf("SEND_OP_COND(CMD1)\n");
	    sdc_start_transaction();
	    sdc_cmd_token(1, 0, 0);
	    r1 = sdc_wait_cmd_resp();
	    sdc_finish_transaction();

	    if(r1 == 0) {
		printf("Identified: MMC v3\n");
		return MMC_V3;
	    } else if(r1 != 1) {
		printf("Error: unknown card\n");
		return SDC_UNKNOWN;
	    }
	}
    } else {
	fprintf(stderr, "Error: no SD card?\n");
	return SDC_UNKNOWN;
    }
}

static void sdc_set_blksize(void)
{
    printf("SET_BLOCKLEN(CMD16)\n");
    sdc_start_transaction();
    sdc_cmd_token(16, 0x200, 0);
    sdc_wait_cmd_resp();
    sdc_finish_transaction();
}

uint8_t sdc_initialize(void)
{
    hperiod.tv_sec = 0;
    hperiod.tv_nsec = SDC_SLOW_CLK / 2;
    sdc_init_spi_mode();
    sdctype = sdc_identify();

    hperiod.tv_nsec = SDC_FAST_CLK / 2;
    if(sdctype == SDC_V2_BYTE || sdctype == SDC_V1 || sdctype == MMC_V3)
	sdc_set_blksize();
}

/*----------------------------------------------------------------------*/

static void print_dump(int len, char *buf)
{
    int i, base;
    printf("       ");
    for(i = 0; i < 16; i++)
	printf("+%1X ", i);
    printf("\n");
    for(base = 0; base < len; base += 16) {
	printf("  %04x ", base);
	for(i = 0; i < 16; i++)
	    printf("%02x ", buf[base + i]);
	for(i = 0; i < 16; i++)
	    printf("%c", (buf[base + i] >= ' ' &&  buf[base + i] <= '~')
		   ? buf[base + i] : '.');
	printf("\n");
    }
}

void sdc_dump_csd(void)
{
    char buf[16];
    printf("SEND_CSD(CMD9)\n");
    sdc_start_transaction();
    sdc_cmd_token(9, 0, 0);
    sdc_wait_cmd_resp();
    sdc_read_data(16, buf);
    sdc_finish_transaction();
    print_dump(16, buf);
}

void sdc_dump_cid(void)
{
    char buf[16];
    printf("SEND_CID(CMD10)\n");
    sdc_start_transaction();
    sdc_cmd_token(10, 0, 0);
    sdc_wait_cmd_resp();
    sdc_read_data(16, buf);
    sdc_finish_transaction();
    print_dump(16, buf);
}

void sdc_read_sector(unsigned sector, char *buf)
{
    unsigned r1, rr;
    printf("READ_SINGLE_BLOCK(CMD17)\n");
    sdc_start_transaction();
    sdc_cmd_token(17, sector, 0);
    r1 = sdc_wait_cmd_resp();
    if(r1 == 0)
	rr = sdc_read_data(512, buf);
    sdc_finish_transaction();
    if(r1 == 0 && rr == 0)
	print_dump(512, buf);
}

void sdc_write_sector(unsigned sector, char *buf)
{
    unsigned r1, rr;
    printf("WRITE_BLOCK(CMD24)\n");
    sdc_start_transaction();
    sdc_cmd_token(24, sector, 0);
    r1 = sdc_wait_cmd_resp();
    if(r1 == 0)
	rr = sdc_write_data(512, buf);
    sdc_finish_transaction();
}

/*----------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    printf("Initializing...\n");
    io_init();
    sdc_initialize();

#if 0
    printf("Dump card info\n");
    sdc_dump_csd();
    sdc_dump_cid();
#endif

#if 1
    char buf[512];
    printf("Sector read\n");
    sdc_read_sector(0, buf);
#endif

#if 0
    buf[0] += 1;
    printf("Sector write\n");
    sdc_write_sector(0, buf);
#endif

    return 0;
}
