/*
 * DL144128TF (128x128 TFT LCD module) sample - uspace version
 *
 *  − This module uses ILI9163C as LCD driver
 *  - Uses 4-wire serial interface
 *  - master SPI is SPI0 of Raspberry Pi
 *
 * Wiring:
 *  DL144128TF  Alias		Raspberry Pi
 *  ----------- ----------	------------
 *  1 VCC	3.3V		17 3.3V
 *  2 GND	GND		20 GND
 *  3 TFT_CS	CSX: CS#	24 GPIO8/CE0
 *  4 REST	RESX: RES#	18 GPIO24
 *  5 TFT_A0	D/CX: D/C#	22 GPIO25
 *  6 TFT_SDA	SPI_MIMO	19 MOSI
 *  7 TFT_SCK	SPI_SCK		23 SCLK
 *  8 LED	3.3V		 1 3.3V
 */

#include <time.h> /* time() */
#include <sys/time.h> /* gettimeofday() */
#include "dl144128tf/dl144128tf.h"

static const struct dl144128tf_config myconfig = {
    .pin_config = {
	.csx = 8,
	.dcx = 25,
	.resx = 24,
    },

    .spi_driver = SPI_USPACE,

    .spi_config = {
	.mode = SPI_MODE_0,
	.speed = 50 * 1000 * 1000, /* 50MHz */
	.flags = SPI_3WIRE | SPI_NO_CS,
	.chip_sel = 3, /* no auto CS assertion */
    },
};

int main(int argc, char *argv[])
{
    srand(time(NULL));
    struct dl144128tf *lcd = dl144128tf_init(&myconfig);

    unsigned char par[128 * 3 * 128];
    unsigned x, y, xx, yy, color;
    for(;;) {
	for(yy = 0; yy < 16; yy++) {
	    for(xx = 0; xx < 16; xx++) {
		color = (rand() >> 2) & 0xffffff;

		for(y = 0; y < 8; y++) {
		    for(x = 0; x < 8; x++) {
			par[((yy*8+y)*128+xx*8+x)*3 + 0] = color;
			par[((yy*8+y)*128+xx*8+x)*3 + 1] = color >> 8;
			par[((yy*8+y)*128+xx*8+x)*3 + 2] = color >> 16;
		    }
		}
	    }
	}

	struct timeval t0, t1;
	gettimeofday(&t0, NULL);
	dl144128tf_write(lcd, ILI9163C_RAMWR, par, sizeof(par));
	gettimeofday(&t1, NULL);
	printf("transfer time = %ldus\n",
	       (t1.tv_sec - t0.tv_sec) * 1000000 + (t1.tv_usec - t0.tv_usec));

	break;
    }

    return 0;
}

