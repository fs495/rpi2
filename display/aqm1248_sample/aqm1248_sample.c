#define CONFIG_IMAGE_TEST 1
#define CONFIG_BAR_GRAPH 1
#define CONFIG_RAW_FONT_TEST 1
#define CONFIG_FONT 1

#include <stdio.h>
#include <unistd.h>
#include "aqm1248.h"

#if defined(__AVR__)
# define msleep(x) _delay_ms(x)
#elif defined(__linux__)
# define msleep(x) usleep((x) * 1000)
#endif

#ifdef CONFIG_IMAGE_TEST
# include "toho-komakyo.c"
#endif

void hw_init(void);

int main(int argc, char *argv[])
{
    hw_init();

    aqm1248_connect_spi();
    aqm1248_init();
    aqm1248_disconnect_spi();

    for(;;) {
	uint8_t i;
	const char *msgs[] = {
	    "One,", "Two,", "Three,", "Four,", "Five,",
	    "Six,", "Seven,", "Eight,", "Nine,", "Ten.",
	};

#if CONFIG_IMAGE_TEST
	/* イメージを表示してスクロール */
	aqm1248_connect_spi();
	aqm1248_write_block(0, 0, IMG_TOHO_KOMAKYO_WIDTH,
			 IMG_TOHO_KOMAKYO_HEIGHT / 8,
			 img_toho_komakyo);
	for(i = 0; i < AQM1248_VRAM_HEIGHT; i++) {
	    aqm1248_set_display_row(i);
	    msleep(50);
	}
	aqm1248_disconnect_spi();
#endif

#if CONFIG_BAR_GRAPH
	aqm1248_connect_spi();
	aqm1248_set_display_row(0);
	for(i = 0; i < AQM1248_WIDTH; i++) {
	    aqm1248_fill_vram(i, 1, 1, 4, 255);
	    msleep(5);
	}
	for(i = 0; i < AQM1248_WIDTH; i++) {
	    aqm1248_fill_vram(i, 1, 1, 4, 0);
	    msleep(5);
	}
	aqm1248_disconnect_spi();
#endif

#if CONFIG_RAW_FONT_TEST
	/* 生のフォントデータ、ブロック転送 */
	aqm1248_connect_spi();
	aqm1248_write_blockp(64, 0, 8, 6, font8x16 + 16);
	for(i = 0; i < 16 * 3; i++) {
	    aqm1248_write_block(8 * (i % 16), 2 * (i / 16), 8, 2,
			     font8x16 + i * 16);
	}
	aqm1248_disconnect_spi();
	msleep(1000);
#endif

#if CONFIG_FONT
	/* フォント込みのスクリーン表示 */
	aqm1248_config_font(ASCII7_8x16);
	aqm1248_line_wrap(1);

	aqm1248_connect_spi();
	aqm1248_clear_screen();

	/* 自動改行 */
	for(i = 0; i < sizeof(msgs) / sizeof(*msgs); i++) {
	    aqm1248_puts(msgs[i]);
	    msleep(500);
	}
	/* 明示的改行 */
	for(i = 0; i < sizeof(msgs) / sizeof(*msgs); i++) {
	    aqm1248_putchar('\n');
	    aqm1248_puts(msgs[i]);
	    msleep(500);
	}
	aqm1248_putchar('\n');

	for(i = 0; i < 100; i++) {
	    char buf[64];
	    sprintf(buf, "%d,", i);
	    aqm1248_puts(buf);
	    msleep(50);
	}
	aqm1248_disconnect_spi();
#endif
    }
}
