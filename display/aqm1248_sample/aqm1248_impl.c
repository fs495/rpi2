/*
 * AQM1248のRaspberry Pi用のサンプル実装
 * 以下のピンアサインを想定している
 *
 * P1 name   AQM1248
 * -- ------ -------
 * 17 3.3V   VDD
 * 19 MOSI   SDI
 * 21 n/c    n/c
 * 7  GPIO4  RS
 * 23 SCLK   SCLK
 * 26 CE1    CS#
 * 25 GND    GND
 */
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include <string.h>
#include <stdio.h>

#include "spi_api.h"
#include "gpio_ureg.h"
#include "aqm1248.h"

/* 液晶モジュールのRS信号に接続するGPIO番号 */
#define GPIO_RS_PIN 4
#define SPI_SPEED (16 * 1000 * 1000)

/*----------------------------------------------------------------------*/

static const struct spi_config_info spi_config_info = {
    .chip_sel = 1,
    .dev = "/dev/spidev0.1",
    .mode = SPI_MODE_1,
    .speed = SPI_SPEED,
    .flags = 0,
};

static struct spi_handle *spih;
static struct gpio_handle gpio;

void hw_init(void)
{
    /* GPIOをRS信号として初期化 */
    if(gpio_init(&gpio) < 0) {
	fprintf(stderr, "Error: gpio_init\n");
	return;
    }
    gpio_select_func(&gpio, GPIO_RS_PIN, GPFSEL_OUTPUT);

    /* SPIの初期化 */
    spih = spi_alloc_bus(&spi_config_info);
}

void hw_fini(void)
{
    gpio_fini(&gpio);
    spi_free_bus(spih);
}

/*----------------------------------------------------------------------*/

void aqm1248_connect_spi(void)
{
    /* なにもしない */
}

void aqm1248_disconnect_spi(void)
{
    /* なにもしない */
}

void aqm1248_select_cmd(void)
{
    gpio_clr(&gpio, GPIO_RS_PIN);
}

void aqm1248_select_data(void)
{
    gpio_set(&gpio, GPIO_RS_PIN);
}

void aqm1248_send_byte(uint8_t byte)
{
    spih->tx_buf = (uintptr_t)&byte;
    spih->rx_buf = (uintptr_t)NULL;
    spih->len = 1;
    if(spi_xfer(spih) < 0)
	fprintf(stderr, "Warn: aqm1248_send_byte\n");
}

void aqm1248_send_block(const uint8_t *p, unsigned len)
{
    spih->tx_buf = (uintptr_t)p;
    spih->rx_buf = (uintptr_t)NULL;
    spih->len = len;
    if(spi_xfer(spih) < 0)
	fprintf(stderr, "Warn: aqm1248_send_block\n");
}

#define HAVE_BLOCK_SPI_TRANSFER

#include "../aqm1248/aqm1248_main_tmpl.c"
