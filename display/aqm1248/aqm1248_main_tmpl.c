/**
 * グラフィック液晶モジュールAQM1248のAPIライブラリ
 */

#if defined(__AVR__)
# include <avr/pgmspace.h>
# include <util/delay.h>
#elif defined(__linux__)
# include <unistd.h>
# define pgm_read_byte(a)	(*(a))
# define _delay_ms(x)		usleep((x) * 1000)
#else
# error ""
#endif

#include "aqm1248.h"

/*======================================================================
 * 初期化
 */

void aqm1248_init(void)
{
    aqm1248_select_cmd();

    aqm1248_send_byte(0xae); /* display = off */
    aqm1248_send_byte(0xa0); /* ADC(address counter?) = normal */
    aqm1248_send_byte(0xc8); /* common output = reverse*/
    aqm1248_send_byte(0xa3); /* LCD bias = 1/7 */

    aqm1248_send_byte(0x2c); /* power control 1 */
    _delay_ms(2);
    aqm1248_send_byte(0x2e); /* power control 2 */
    _delay_ms(2);
    aqm1248_send_byte(0x2f); /* power control 3 */

    aqm1248_send_byte(0x23);
    aqm1248_send_byte(0x81);
    aqm1248_send_byte(0x19);

    aqm1248_send_byte(0xa4); /* display all point = normal*/
    aqm1248_send_byte(0x40); /* display start line = 0 */
    aqm1248_send_byte(0xa6); /* common output = normal */
    aqm1248_send_byte(0xaf); /* display = on */

    aqm1248_clear_vram();
}

/*======================================================================
 * 低レベルコマンドの発行
 * 呼び出し前にコマンド送信可能(SPI有効、SSアサート済、RS=コマンド)であること
 */

void aqm1248_display_on(void)
{
    aqm1248_send_byte(0xae);
}

void aqm1248_display_off(void)
{
    aqm1248_send_byte(0xaf);
}

void aqm1248_set_display_row(uint8_t row)
{
    aqm1248_send_byte(0x40 | row);
}

void aqm1248_set_addr_page(uint8_t page)
{
    aqm1248_send_byte(0xb0 | page);
}

void aqm1248_set_addr_col(uint8_t col)
{
    aqm1248_send_byte(0x10 | (col >> 4));
    aqm1248_send_byte(0x00 | (col & 0xf));
}

void aqm1248_set_resistor_ratio(uint8_t val)
{
    aqm1248_send_byte(0x20 | val);
}

void aqm1248_set_contrast(uint8_t val)
{
    aqm1248_send_byte(0x81);
    aqm1248_send_byte(val);
}

void aqm1248_set_sleep_mode(void)
{
    aqm1248_send_byte(0xac);
    aqm1248_send_byte(0x00);
}

void aqm1248_leave_sleep_mode(void)
{
    aqm1248_send_byte(0xad);
    aqm1248_send_byte(0x00);
}

/*======================================================================
 * ブロック書き込み・ブロックフィル
 */

/*
 * ブロックデータを書き込む
 */
void aqm1248_write_block(uint8_t sx, uint8_t sy, uint8_t w, uint8_t h,
		      const uint8_t *p)
{
    for(uint8_t y = 0; y < h; y++) {
	aqm1248_select_cmd();
	aqm1248_set_addr_page(sy + y);

	aqm1248_set_addr_col(sx);
	aqm1248_select_data();
#ifdef HAVE_BLOCK_SPI_TRANSFER
	aqm1248_send_block(p, w);
	p += w;
#else
	for(uint8_t x = 0; x < w; x++)
	    aqm1248_send_byte(*p++);
#endif
    }
    aqm1248_select_cmd();
}

/*
 * ブロックデータを書き込む
 */
void aqm1248_write_blockp(uint8_t sx, uint8_t sy, uint8_t w, uint8_t h,
		       const uint8_t *p)
{
    for(uint8_t y = 0; y < h; y++) {
	aqm1248_select_cmd();
	aqm1248_set_addr_page(sy + y);

	aqm1248_set_addr_col(sx);
	aqm1248_select_data();
#ifdef HAVE_BLOCK_SPI_TRANSFER
	aqm1248_send_block(p, w);
	p += w;
#else
	for(uint8_t x = 0; x < w; x++)
	    aqm1248_send_byte(pgm_read_byte(p++));
#endif
    }
    aqm1248_select_cmd();
}

/*
 * 表示メモリを指定値でフィルする
 */
void aqm1248_fill_vram(uint8_t sx, uint8_t sy, uint8_t w, uint8_t h, uint8_t ptn)
{
    uint8_t x, y;
    for(y = 0; y < h; y++) {
	aqm1248_select_cmd();
	aqm1248_set_addr_page(sy + y);

	aqm1248_set_addr_col(sx);
	aqm1248_select_data();
	for(x = 0; x < w; x++)
	    aqm1248_send_byte(ptn);
    }
    aqm1248_select_cmd();
}

/*
 * 表示メモリをクリアする
 */
void aqm1248_clear_vram(void)
{
    aqm1248_fill_vram(0, 0, AQM1248_WIDTH, AQM1248_VRAM_PAGES, 0);
}

