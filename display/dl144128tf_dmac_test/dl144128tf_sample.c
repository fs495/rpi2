/*
 * DL144128TF (128x128 TFT LCD module) sample - DMA version
 *
 *  − This module uses ILI9163C as LCD driver
 *  - Uses 4-wire serial interface
 *  - master SPI is SPI0 of Raspberry Pi
 *
 * Wiring:
 *  DL144128TF  Alias		Raspberry Pi
 *  ----------- ----------	------------
 *  1 VCC	3.3V		17 3.3V
 *  2 GND	GND		20 GND
 *  3 TFT_CS	CSX: CS#	24 CE0
 *  4 REST	RESX: RES#	18 GPIO24
 *  5 TFT_A0	D/CX: D/C#	22 GPIO25
 *  6 TFT_SDA	SPI_MIMO	19 MOSI
 *  7 TFT_SCK	SPI_SCK		23 SCLK
 *  8 LED	3.3V		 1 3.3V
 */

#define RESX_PIN 24
#define DCX_PIN  25
#define CSX_PIN  8

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h> /* time() */
#include <sys/time.h>

#include "gpio/uspace_gpio.h"
#include "spi/spi.h"
#include "dl144128tf/ili9163c.h"

static struct uspace_iop_handle *gpioh;
static struct spi_handle *spih;

static struct spi_config_info spi_config = {
    .mode = SPI_MODE_0,
    .speed = 50 * 1000 * 1000,
    .flags = SPI_3WIRE | SPI_NO_CS,
    .dev = "/dev/spidev0.0",
    .chip_sel = 3, /* no auto CS assertion */
    .maxlen = 128 * 128 * 3,
    .tx_dma_chan = 4,
    .rx_dma_chan = 5,
};

static void write_cycle(uint8_t cmd, const uint8_t *param, size_t paramlen)
{
    gpio_clr(gpioh, CSX_PIN);
    gpio_clr(gpioh, DCX_PIN);

    /* send command byte */
    *(uint8_t*)spih->tx_memh->virt_addr = cmd;
    spih->len = 1;
    spidma_xfer(spih);

    /* send paramater bytes if it exists */
    if(param != NULL && paramlen > 0) {
	gpio_set(gpioh, DCX_PIN);
	memcpy(spih->tx_memh->virt_addr, param, paramlen);
	spih->len = paramlen;
	spidma_xfer(spih);
    }

    gpio_set(gpioh, CSX_PIN);
}

static void init_dl144128tf(void)
{
    unsigned char par[6];

    /* initialize */
    spih = spidma_alloc_bus(&spi_config);

    gpioh = gpio_alloc_uspace();
    gpio_select_func(gpioh, RESX_PIN, GPFSEL_OUTPUT);
    gpio_select_func(gpioh, DCX_PIN, GPFSEL_OUTPUT);
    gpio_select_func(gpioh, CSX_PIN, GPFSEL_OUTPUT);

    /* hardware reset */
    gpio_set(gpioh, CSX_PIN);
    usleep(1000);
    gpio_clr(gpioh, RESX_PIN);
    usleep(10000);
    gpio_set(gpioh, RESX_PIN);
    usleep(50000);

    /* Software Reset */
    write_cycle(ILI9163C_SWRESET, NULL, 0);
    usleep(1000);

    /* Sleep Out */
    write_cycle(ILI9163C_SLPOUT, NULL, 0);
    usleep(20000);

    /* Set default gamma */
    par[0] = 4;
    write_cycle(ILI9163C_GAMMASET, par, 1);

    par[0] = 0;
    write_cycle(ILI9163C_GAMRSEL, par, 1);

    par[0] = 0x0a; par[1] = 0x14;
    write_cycle(ILI9163C_FRMCTR1, par, 2);

#if 0
    par[0] = 0x0d; par[1] = 05;
    write_cycle(ILI9163C_PWCTR1, par, 2);

    par[0] = 2;
    write_cycle(ILI9163C_PWCTR2, par, 1);

    par[0] = 0x35; par[1] = 0x37;
    write_cycle(ILI9163C_VCOMCTR1, par, 2);

    par[0] = 0x40;
    write_cycle(ILI9163C_VCOMOFFS, par, 1);
#endif

    /* column address set */
    par[0] = 0; par[1] = 0; par[2] = 0; par[3] = 127;
    write_cycle(ILI9163C_CLMADRS, par, 4);

    /* page address set */
    par[3] = 159;
    write_cycle(ILI9163C_PGEADRS, par, 4);

    /* Memory Access Control */
    par[0] = 0xc0;
    write_cycle(ILI9163C_MADCTL, par, 1);

    /* Set Color Format */
    par[0] = 6;
    write_cycle(ILI9163C_PIXFMT, par, 1);

    /* Set Vertical Scroll Area */
    par[0] = par[1] = 0;
    par[2] = 0; par[3] = 128;
    par[4] = par[5] = 0;
    write_cycle(ILI9163C_VSCLLDEF, par, 6);

    /* Display On */
    write_cycle(ILI9163C_DISPON, NULL, 0);
}

int main(int argc, char *argv[])
{
    srand(time(NULL));
    init_dl144128tf();

    dmac_print(&spih->tx_dmah);
    dmac_print(&spih->rx_dmah);

    unsigned char par[128 * 128 * 3];
    unsigned x, y, xx, yy, color;
    for(;;) {
	for(yy = 0; yy < 16; yy++) {
	    for(xx = 0; xx < 16; xx++) {
		color = (rand() >> 2) & 0xffffff;

		for(y = 0; y < 8; y++) {
		    for(x = 0; x < 8; x++) {
			par[((yy*8+y)*128+xx*8+x)*3 + 0] = color;
			par[((yy*8+y)*128+xx*8+x)*3 + 1] = color >> 8;
			par[((yy*8+y)*128+xx*8+x)*3 + 2] = color >> 16;
		    }
		}
	    }
	}

	struct timeval t0, t1;
	gettimeofday(&t0, NULL);
	write_cycle(ILI9163C_RAMWR, par, sizeof(par));
	gettimeofday(&t1, NULL);
	printf("transfer time = %ldus\n",
	       (t1.tv_sec - t0.tv_sec) * 1000000 + (t1.tv_usec - t0.tv_usec));

	break;
    }

    return 0;
}
