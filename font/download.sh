#!/bin/sh

fetch() {
    file=$1
    url=$2
    if [ -f $file ]; then
	echo "Skipped: $file already exists"
    else
	echo "Fetching $file from $url"
	wget -O "$file" "$url"
    fi
}

case $1 in
    download_font)
	cd work_font
	fetch K14-2004-1.bdf.gz \
	      'https://web.archive.org/web/20141006232653/http://www12.ocn.ne.jp/~imamura/K14-2004-1.bdf.gz'
	fetch Kappa20-0.396.tar.bz2 \
	      'http://web.archive.org/web/20101221113130/http://kappa.allnet.ne.jp/20dot.fonts/Kappa20-0.396.tar.bz2'
	fetch k12x10bdf.tar.gz \
	      'http://z.apps.atjp.jp/k12x10/k12x10bdf.tar.gz'
	fetch intlfonts-1.2.1.tar.gz \
	      'http://ftp.gnu.org/gnu/intlfonts/intlfonts-1.2.1.tar.gz'
	fetch jiskan16-2004-1.bdf.gz \
	      'https://web.archive.org/web/20141006232653/http://www12.ocn.ne.jp/~imamura/jiskan16-2004-1.bdf.gz'
	fetch milkjf_bdf.tar.gz \
	      'http://web.archive.org/web/20061212224857/http://phe.phyas.aichi-edu.ac.jp/~cyamauch/arch/milkjf_bdf.tar.gz'
	fetch mplus_bitmap_fonts-2.2.4.tar.gz \
	      'http://downloads.osdn.jp/mplus-fonts/5030/mplus_bitmap_fonts-2.2.4.tar.gz'
	fetch shinonome12-1.0.9.tar.bz2 \
	      'http://openlab.ring.gr.jp/efont/dist/shinonome/orig/shinonome12-1.0.9.tar.bz2'
	fetch shinonome16-1.0.4.tar.bz2 \
	      'http://openlab.ring.gr.jp/efont/dist/shinonome/orig/shinonome16-1.0.4.tar.bz2'
	;;

    extract_font)
	cd work_font

	rm -rf K14-2004-1; mkdir K14-2004-1
	gzip -dc K14-2004-1.bdf.gz > K14-2004-1/K14-2004-1.bdf

	rm -rf Kappa20-0.396
	tar jxf Kappa20-0.396.tar.bz2

	rm -rf intlfonts-1.2.1
	tar zxf intlfonts-1.2.1.tar.gz

	rm -rf jiskan16-2004-1; mkdir jiskan16-2004-1
	gzip -dc jiskan16-2004-1.bdf.gz > jiskan16-2004-1/jiskan16-2004-1.bdf

	rm -rf milkjf_bdf; mkdir milkjf_bdf
	(cd milkjf_bdf && tar zxf ../milkjf_bdf.tar.gz)

	rm -rf mplus_bitmap_fonts-2.2.4
	tar zxf mplus_bitmap_fonts-2.2.4.tar.gz

	rm -rf shinonome12-1.0.9
	tar jxf shinonome12-1.0.9.tar.bz2

	rm -rf shinonome16-1.0.4
	tar jxf shinonome16-1.0.4.tar.bz2
	;;

    download_etc)
	cd work_etc
	fetch CP932.TXT \
	      'http://unicode.org/Public/MAPPINGS/VENDORS/MICSFT/WINDOWS/CP932.TXT'
	;;
esac
