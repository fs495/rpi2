#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require './bdf2'

TESTFONT = 'work_font/intlfonts-1.2.1/Japanese.X/8x16rk.bdf'
def test_load_bdf()
    bdf = BDF.new()
    # 8x16rkは190文字のデータがある
    bdf.load_bdffile(TESTFONT)
    raise if bdf.glyphs.size != 190
    puts "test_load_bdf: OK"
end

def test_find_segments()
    bdf = BDF.new()
    bdf.load(TESTFONT)
    res = bdf.char_segs
    raise if res.size != 2
    raise if res[0] != BDF::CharRange.new(0x01, 0x7e, 8)
    raise if res[1] != BDF::CharRange.new(0xa0, 0xdf, 8)
    puts "test_find_segments: OK"
end

def test_rotate_ccw()
    ptn = [
	0b11110000,
	0b11001100,
	0b10101010,
	0b10001000,
	0b10000000,
	0b11000000,
	0b11100000,
	0b11111111 ]
    expected = [
	0b11111111,
	0b11100011,
	0b11000101,
	0b10000001,
	0b10001110,
	0b10000010,
	0b10000100,
	0b10000000 ]

    glyph = BDF::Glyph.new
    glyph.bbxw = 8
    glyph.bbxh = 8

    # 横8 x 縦8
    glyph.ptn = ptn
    res = glyph.rotate_ccw()
    raise if res != expected

    # 横8 x 縦16
    glyph.ptn = ptn + [0] * 8
    glyph.bbxh = 16
    res = glyph.rotate_ccw()
    raise if res != expected + [0] * 8

    # 横16 x 縦8
    glyph.ptn = ptn
    glyph.bbxw = 16
    glyph.bbxh = 8
    res = glyph.rotate_ccw()
    raise if res != [0] * 8 + expected

    puts "test_rotate_ccw: OK"

    # 横16 x 縦16
    glyph.bbxh = 16
    glyph.ptn = ptn + [0] * 8
    res = glyph.rotate_ccw()
    raise if res != [0] * 8 + expected + [0] * 16
end

if __FILE__ == $0
    # ユニットテスト
    test_load_bdf()
    test_find_segments()
    test_rotate_ccw()
end
