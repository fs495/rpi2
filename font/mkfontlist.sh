#!/bin/sh

rm -f work_out/font_*.txt

find work_font -name \*.bdf | while read path
do
    ruby bdf2.rb -s $path | tee tmp
    if egrep -i 'charset: (jisx0208|jisx0212|jisx0213)' tmp > /dev/null; then
	head -6 tmp >> work_out/font_jpn.txt

    elif egrep -i 'charset: (JISX0201|iso8859)' tmp > /dev/null; then
	head -6 tmp >> work_out/font_ascii.txt
    fi
done

rm -f tmp
