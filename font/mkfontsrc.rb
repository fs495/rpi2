#!/usr/bin/env ruby

require 'pp'
require 'optparse'
require './bdf2'

class BDF
    def check_7bit_ascii()
	seg0 = @char_segs[0]
	raise "first segment must start <= 32 (#{seg0})" if seg0.start > 32
	raise "first segment must end >= 126 (#{seg0})" if seg0.last <126
    end

    def output_header(out, identifier)
	out.puts "#if defined(__AVR__)"
	out.puts "# include <avr/pgmspace.h>"
	out.puts "#elif !defined(PROGMEM)"
	out.puts "# define PROGMEM /* empty */"
	out.puts "#endif"
	out.puts ""
	out.puts "const unsigned char #{identifier}[] PROGMEM = {"
    end

    def output_footer(out)
	out.puts "};"
    end

    class Glyph
	def output_body(out)
	    s = "\t/* 0x#{@code.to_s(16)}(#{@code}) */\n\t"
	    @ptn.each do |p|
		((@bbxw + 7) / 8).times do |octet|
		    s += ((p >> (8 * octet)) & 255).to_s
		    s += ','
		end
	    end
	    out.puts s
	end
    end

    def output(out, opt)
	output_header(out, opt[:identifier])
	char_segs.each do |seg|
	    s, l = seg.start, seg.last
	    s, l = 32, 126 if opt[:is_ascii]
	    (s..l).each do |code|
		glyphs[code].output_body(out)
	    end
	    break if opt[:is_ascii]
	end
	output_footer(out)
    end
end

#----------------------------------------------------------------------
# Usage: ruby mkfontsrc.rb [-a] bdffile outfile identifier

opt = {}

if ARGV.size > 0 && ARGV[0] == '-a'
    opt[:is_ascii] = true
    ARGV.shift
end
opt[:bdffile] = ARGV.shift
opt[:outfile] = ARGV.shift
opt[:identifier] = ARGV.shift

bdf = BDF.new
bdf.load(opt[:bdffile])
bdf.check_7bit_ascii if opt[:is_ascii]
open(opt[:outfile], "w") do |out|
    bdf.output(out, opt)
end
