#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'optparse'
require 'pp'

class BDF
    attr_accessor :prop, :char_segs, :glyphs

    #----------------------------------------------------------------------
    class Properties
	attr_accessor :gbbxw, :gbbxh, :charset, :fixed_width
    end

    class CharRange
	attr_accessor :start, :last, :width

	def initialize(start, last, width)
	    @start, @last, @width = start, last, width
	end

	def to_s
	    return "start=#{@start} last=#{@last} width=#{@width}"
	end

	def ==(x)
	    return @start == x.start && @last == x.last && @width == x.width
	end
    end

    class Glyph
	attr_accessor :code, :bbxw, :bbxh, :ptn

	# グリフを反時計回りに90度回転したパターンのArrayを返す
	def rotate_ccw()
	    bpr = (@bbxw + 7) / 8
	    work = nil
	    out = []

	    # 横1ラインずつスキャン
	    (0 ... @bbxh).each do |y|
		#puts "in: " + ('0' * @bbxw + ptn[y].to_s(2))[-@bbxw, @bbxw]

		# 回転しつつ出力データにビットをコピーしていく
		if y % 8 == 0
		    work = [0] * @bbxw
		end
		(0 ... @bbxw).each do |x|
		    if (ptn[y] & (1 << (@bbxw - 1 - x))) == 0
			work[x] = (work[x] >> 1)
		    else
			work[x] = (work[x] >> 1) | 128
		    end
		end

		# 縦8ドットごとに出力する
		if (y + 1) % 8 == 0
		    (0 ... @bbxw).each do |x|
			#puts ('00000000' + work[x].to_s(2))[-8, 8]
		    end
		    out << work
		end
	    end
	    return out.flatten
	end
    end

    #----------------------------------------------------------------------
    def initialize
	@prop = Properties.new
	@char_segs = []
	@glyphs = {}
    end

    def load_bdffile(bdffile)
	state = nil
	g = Glyph.new

	open(bdffile) do |fd|
	    while buf = fd.gets
		buf.chomp!

		case state
		when nil
		    case buf
		    when /^CHARSET_REGISTRY "(.*)"/
			@prop.charset = $1
		    when /^FONTBOUNDINGBOX (\d+) (\d+) (\d+) (-?\d+)/
			@prop.gbbxw, @prop.gbbxh = $1.to_i, $2.to_i
		    when /^STARTCHAR/
			state = :char_header
		    end

		when :char_header
		    case buf
		    when /^ENCODING (\d+)/
			g.code = $1.to_i # BDFファイル中なので10進表記のJIS区点コード
		    when /^BBX (\d+) (\d+) (\d+) (-?\d+)/
			g.bbxw, g.bbxh = $1.to_i, $2.to_i
			raise "縦サイズが固定値ではない" if g.bbxh != @prop.gbbxh
		    when /^BITMAP/
			state = :char_bitmap
			g.ptn = []
		    end

		when :char_bitmap
		    if buf == 'ENDCHAR'
			raise "縦サイズがBBX文と異なる" if g.ptn.length != @prop.gbbxh
			@glyphs[g.code] = g
			g = Glyph.new
			state = :char_header
		    else
			raise "横サイズがBBX文と異なる" if buf.length != (g.bbxw + 7) / 8 * 2
			g.ptn << buf.hex
		    end
		end
	    end
	end
    end

    # ロードしたBDFファイル情報に対し、グリフ横幅が同じで
    # かつコードポイントが連続するブロックを見つける。
    def find_segments()
	state = 0
	start_code, last_code, last_width = nil, nil, nil

	@glyphs.keys.sort.each do |code|
	    case state
	    when 0
		state = 1
		start_code, last_code, last_width = code, code, @glyphs[code].bbxw

	    when 1
		if code == last_code + 1 && @glyphs[code].bbxw == last_width
		    last_code = code
		else
		    @char_segs << CharRange.new(start_code, last_code, last_width)
		    start_code, last_code, last_width = code, code, @glyphs[code].bbxw
		end
	    end
	end

	@char_segs << CharRange.new(start_code, last_code, last_width)
	@prop.fixed_width = @char_segs.all?{|x| x.width == last_width}
    end

    def load(path)
	load_bdffile(path)
	find_segments()
    end
end

#----------------------------------------------------------------------
if __FILE__ == $0
    params = ARGV.getopts("s")

    # BDFファイルを読み込んでサマリを出力
    ARGV.each do |path|
	bdf = BDF.new
	bdf.load(path)

	puts "BDF file path: #{path}"
	puts " charset: #{bdf.prop.charset}"
	puts " fixed width: #{bdf.prop.fixed_width}"
	puts " number of chars: #{bdf.glyphs.size}"
	puts " number of segments: #{bdf.char_segs.size}"
	if params['s']
	    bdf.char_segs.each do |r|
		puts "  " + r.to_s
	    end
	end
    end
end
