#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <getopt.h>

#include "spi_api.h"

static struct spi_config_info config_info = {
    .dev = "/dev/spidev0.1",
    .chip_sel = 1,
    .mode = SPI_MODE_0,
    .speed = 4000000,
};

static struct spi_handle *spih;

static unsigned pcf2129_xfer(unsigned cmd, unsigned data)
{
    unsigned char in[2], out[2];

    out[0] = cmd;
    out[1] = data;
    spih->rx_buf = (uintptr_t)in;
    spih->tx_buf = (uintptr_t)out;
    spih->len = 2;

    int ret = spi_xfer(spih);
    if(ret < 0) {
	fprintf(stderr, "Error: SPI_IOC_MESSAGE failed\n");
	return -1;
    }
    return in[1];
}

unsigned pcf2129_read(unsigned reg)
{
    return pcf2129_xfer(0xa0 | (reg & 0x1f), 0);
}

void pcf2129_write(unsigned reg, unsigned value)
{
    pcf2129_xfer(0x20 | (reg & 0x1f), value);
}

unsigned bcd(unsigned val)
{
    return (val / 10) << 4 | (val % 10);
}

/*----------------------------------------------------------------------*/

void print_reg(void)
{
    printf("current yy/mm/dd is %02x/%02x/%02x\n",
	   pcf2129_read(9), pcf2129_read(8), pcf2129_read(6));
    printf("current hh:mm:ss is %02x:%02x:%02x\n",
	   pcf2129_read(5), pcf2129_read(4), pcf2129_read(3));
}

void dump_raw_reg(void)
{
    int reg;
    printf("Reg  Hex  Dec\n");
    printf("---- ---- ------\n");
    for(reg = 0; reg < 0x1c; reg++) {
	unsigned v = pcf2129_read(reg);
	printf("0x%02x 0x%02x (%d)\n", reg, v, v);
    }
}

void set_current(void)
{
    time_t cur;
    struct tm *utc;

    cur = time(NULL);
    utc = gmtime(&cur);

    pcf2129_write(0, 0);
    pcf2129_write(3, bcd(utc->tm_sec));
    pcf2129_write(4, bcd(utc->tm_min));
    pcf2129_write(5, bcd(utc->tm_hour));
    pcf2129_write(6, bcd(utc->tm_mday));
    pcf2129_write(7, utc->tm_wday);
    pcf2129_write(8, bcd(utc->tm_mon + 1));
    pcf2129_write(9, bcd(utc->tm_year % 100));
}

void clear_flags(void)
{
    pcf2129_write(2, 0);
}

void set_alarm(const char *s)
{
    /* TODO */
}

/*----------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    int opt;

    spih = spi_alloc_bus(&config_info);

    while((opt = getopt(argc, argv, "pdsca:")) != -1) {
	switch(opt) {
	case 'p':
	    print_reg();
	    break;

	case 'd':
	    dump_raw_reg();
	    break;

	case 's':
	    set_current();
	    break;

	case 'c':
	    clear_flags();
	    break;

	case 'a':
	    set_alarm(optarg);
	    break;

	default:
	    fprintf(stderr, "Usage: %s [-pdsh] [-a xxx]\n", argv[0]);
	    exit(1);
	}
    }

    return 0;
}
