#!/bin/sh

INTERVAL=120

while true; do
    echo '====='
    date
    sudo ./ds3231 -pt
    sleep $INTERVAL
done
