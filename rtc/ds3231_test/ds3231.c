#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <getopt.h>

enum {
    NR_REGS = 19
};

static struct {
    int fd;
    int instance;
    unsigned addr;
    uint8_t buf[NR_REGS];
} dev;

unsigned to_bcd(unsigned val)
{
    return (val / 10) << 4 | (val % 10);
}

int open_dev(void)
{
    char path[128];
    sprintf(path, "/dev/i2c-%d", dev.instance);

    dev.fd = open(path, O_RDWR);
    if(dev.fd < 0) {
	fprintf(stderr, "Error: cannot open device: %s\n", path);
	return -1;
    }

    if(ioctl(dev.fd, I2C_SLAVE, dev.addr) < 0) {
	fprintf(stderr, "Error: cannot use slave address 0x%x\n", dev.addr);
	return -1;
    }

    dev.buf[0] = 0; /* point register 0, no actual write */
    if(write(dev.fd, dev.buf, 1) < 0) {
	fprintf(stderr, "Error: cannot set pointer\n");
	return -1;
    }

    if(read(dev.fd, dev.buf, sizeof(dev.buf)) < 0) {
	fprintf(stderr, "Error: cannot read\n");
	return -1;
    }

    return 0;
}

/*----------------------------------------------------------------------*/

void print_curr(void)
{
    uint8_t *p = dev.buf;
    printf("current date/time is %04x/%02x/%02x %02x:%02x:%02x\n",
	   p[6] + 0x2000, p[5], p[4], p[2], p[1], p[0]);
}

void dump_raw_reg(void)
{
    printf("     Hex  Dec\n");
    printf("     ---- ------\n");
    for(int reg = 0; reg < NR_REGS; reg++) {
	uint8_t raw = dev.buf[reg];
	printf("0x%02x 0x%02x %d\n", reg, raw, raw);
    }
}

void set_current(void)
{
    time_t cur;
    struct tm *tm;

    cur = time(NULL);
    tm = localtime(&cur);
    dev.buf[0] = to_bcd(tm->tm_sec);
    dev.buf[1] = to_bcd(tm->tm_min);
    dev.buf[2] = to_bcd(tm->tm_hour);
    dev.buf[3] = tm->tm_wday + 1;
    dev.buf[4] = to_bcd(tm->tm_mday);
    dev.buf[5] = to_bcd(tm->tm_mon + 1);
    dev.buf[6] = to_bcd(tm->tm_year % 100);

    char buf[8];
    buf[0] = 0;
    memcpy(buf + 1, dev.buf, 7);
    if(write(dev.fd, buf, 8) < 0)
	fprintf(stderr, "Error: write failed\n");
}

void show_temperature(void)
{
    printf("current temperature is %d.%d deg C\n",
	   dev.buf[17], dev.buf[18] * 25 / 0x40);
}

/*----------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    int opt;

    /* デフォルト値をあらかじめセット */
    dev.instance = 1;
    dev.addr = 0x68;

    while((opt = getopt(argc, argv, "D:A:pdts")) != -1) {
	switch(opt) {
	case 'D':
	    dev.instance = atoi(optarg);
	    break;

	case 'A':
	    dev.addr = atoi(optarg);
	    break;

	case 'p':
	    if(open_dev() >= 0)
		print_curr();
	    break;

	case 'd':
	    if(open_dev() >= 0)
		dump_raw_reg();
	    break;

	case 't':
	    if(open_dev() >= 0)
		show_temperature();
	    break;

	case 's':
	    if(open_dev() >= 0)
		set_current();
	    break;

	default:
	    fprintf(stderr, "Usage: %s [-D bus_number] [-A address] [-pdts]\n", argv[0]);
	    fprintf(stderr, " -p show current, -d dump, -t show temperature, -s set clock\n");
	    exit(1);
	}
    }

    return 0;
}
