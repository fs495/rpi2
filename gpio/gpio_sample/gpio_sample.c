#include <stdint.h>
#include <stddef.h>
#include <time.h>
#include "gpio/uspace_gpio.h"

#define SIG 21

int main(int argc, char *argv[])
{
    struct uspace_iop_handle *ioph;
    struct timespec tv = {0, 1000};

    ioph = gpio_alloc_uspace();
    gpio_select_func(ioph, SIG, GPFSEL_OUTPUT);

    for(;;) {
	gpio_set(ioph, SIG);
	nanosleep(&tv, NULL);
	gpio_clr(ioph, SIG);
	nanosleep(&tv, NULL);
    }

    return 0;
}
