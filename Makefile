DIRS = lib \
	adconv circuit display font gpio net rtc storage

all:
	for d in $(DIRS); do (cd $$d && test -f Makefile && $(MAKE) all); done

clean:
	for d in $(DIRS); do (cd $$d && test -f Makefile && $(MAKE) clean); done
