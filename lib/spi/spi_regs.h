#ifndef __spi_regs_h__
#define __spi_regs_h__

enum spi_reg_offset {
    SPI_CS   = 0x00,	/* CS SPI Master Control and Status */
    SPI_FIFO = 0x04,	/* SPI Master TX and RX FIFOs */
    SPI_CLK  = 0x08,	/* SPI Master Clock Divider */
    SPI_DLEN = 0x0c,	/* SPI Master Data Length */
    SPI_LTOH = 0x10,	/* SPI LOSSI mode TOH */
    SPI_DC   = 0x14,	/* SPI DMA DREQ Controls */
};

enum spi_cs_value {
    SPICS_CSPOL2 = 1 << 23,
    SPICS_CSPOL1 = 1 << 22,
    SPICS_CSPOL0 = 1 << 21,
    SPICS_RXF = 1 << 20,	/* Read-FIFO full */
    SPICS_RXR = 1 << 19,	/* Read-FIFO needs reading */
    SPICS_TXD = 1 << 18,	/* Send-FIFO can accept data */
    SPICS_RXD = 1 << 17,	/* Read-FIFO contain data */
    SPICS_DONE = 1 << 16,	/* Transfer done */
    SPICS_REN = 1 << 12,	/* Read Enable in bidirectional mode */
    SPICS_ADCS = 1 << 11,	/* Automatic deassert CS */
    SPICS_INTR = 1 << 10,	/* Interrupt on RXR */
    SPICS_INTD = 1 << 9,	/* Interrupt on Done */
    SPICS_DMAEN = 1 << 8,	/* DMA enable */
    SPICS_TA = 1 << 7,		/* Transfer active */
    SPICS_CSPOL = 1 << 6,	/* CS polarity */
    SPICS_CLEAR_TX = 1 << 5,	/* Clear TX FIFO */
    SPICS_CLEAR_RX = 1 << 4,	/* Clear RX FIFO */
    SPICS_CPOL = 1 << 3,	/* Clock polarity */
    SPICS_CPHA = 1 << 2,	/* Clock phase */
};

#define SPI_OFFSET 0x00204000
#define SPI_SEGSIZE 0x18

#endif /* __spi_regs_h__ */
