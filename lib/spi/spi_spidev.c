/*
 * SPI API using spidev driver
 *
 * ref: https://www.kernel.org/doc/Documentation/spi/spidev
 *
 * - This does not support bidir mode using MIMO pin only
 */

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#include "spi.h"

/* spidev seems to have limitation on I/O size for single operation */
#define MAX_IOSIZE 4096

/*----------------------------------------------------------------------*/
struct spi_handle *spidev_alloc_bus(const struct spi_config_info *c)
{
    struct spi_handle *spih = malloc(sizeof(struct spi_handle));
    if(spih == NULL)
	return NULL;

    spih->fd = open(c->dev, O_RDWR);
    if(spih->fd < 0) {
	fprintf(stderr, "Error: open(\"%s\")\n", c->dev);
	free(spih);
	return NULL;
    }

    uint8_t bits = 8;
    int ret = ioctl(spih->fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_BITS_PER_WORD, %d)\n", bits);
	goto err_close_ret;
    }

    ret = ioctl(spih->fd, SPI_IOC_WR_MAX_SPEED_HZ, &c->speed);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_MAX_SPEED_HZ, %d)\n",
		c->speed);
	goto err_close_ret;
    }

    ret = ioctl(spih->fd, SPI_IOC_WR_MODE, &c->mode);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_MODE, %d)\n", c->mode);
	goto err_close_ret;
    }

    return spih;

  err_close_ret:
    close(spih->fd);
    free(spih);
    return NULL;
}

int spidev_free_bus(struct spi_handle *spih)
{
    close(spih->fd);
    free(spih);
    return 0;
}

int spidev_xfer(struct spi_handle *spih)
{
    struct spi_ioc_transfer tr;
    memset(&tr, 0, sizeof(tr));
    tr.rx_buf = spih->rx_buf;
    tr.tx_buf = spih->tx_buf;

    size_t resid = spih->len;
    while(resid > 0) {
	tr.len = resid > MAX_IOSIZE ? MAX_IOSIZE : resid;
	int ret = ioctl(spih->fd, SPI_IOC_MESSAGE(1), &tr);
	if(ret < 0) {
	    fprintf(stderr, "Error: ioctl(SPI_IOC_MESSAGE) failed\n");
	    return ret;
	}

	if(tr.rx_buf) tr.rx_buf += tr.len;
	if(tr.tx_buf) tr.tx_buf += tr.len;
	resid -= tr.len;
    }

    return 0;
}
