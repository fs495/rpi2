/*
 * SPI API using mmaped Raspberry pi SPI registers
 *
 * ref: kernel mode driver source
 * https://github.com/raspberrypi/linux/blob/rpi-3.6.y/drivers/spi/spi-bcm2708.c
 */

#include <unistd.h>

#include "gpio/uspace_gpio.h"

#include "spi.h"
#include "spi_regs.h"

#define RPI_CLOCK (250L * 1000 * 1000)

/*----------------------------------------------------------------------*/

static inline
uint32_t spi_read_reg(struct spi_handle *spih, uintptr_t regoff)
{
    return *(volatile uint32_t*)(spih->regh->virt_addr + regoff);
}

static inline
void spi_write_reg(struct spi_handle *spih, uintptr_t regoff, uint32_t value)
{
    *(volatile uint32_t*)(spih->regh->virt_addr + regoff) = value;
}

/*----------------------------------------------------------------------*/

struct spi_handle *spiuspace_alloc_bus(const struct spi_config_info *c)
{
    struct spi_handle *spih = malloc(sizeof(struct spi_handle));
    if(spih == NULL)
	return NULL;

    /* setup alt func for GPIO7..GPIO11 */
    struct uspace_iop_handle *gpioh = gpio_alloc_uspace();
    if(gpioh == NULL) {
	free(spih);
	return NULL;
    }
    for(int i = 7; i <= 11; i++) {
	gpio_select_func(gpioh, i, GPFSEL_INPUT);
	gpio_select_func(gpioh, i, GPFSEL_ALT0);
    }
    uspace_iop_free(gpioh);

    /* allocate SPI register handle */
    struct uspace_iop_handle *regh = uspace_iop_alloc(SPI_OFFSET, SPI_SEGSIZE);
    if(regh == NULL) {
	free(spih);
	return NULL;
    }
    spih->regh = regh;

    /* see http://www.dibase.co.uk/blog-bcm2835-spi0-doc-clarification
     * for CS (Chip Select) consideration.
     * cs=2 or cs=3 can be used for "no auto CS assertion" */

    /* make base SPI_CS register value */
    spih->csreg = c->chip_sel & 3; /* copy lower 2 bits only */
    if(c->mode & SPI_CPHA)
	spih->csreg |= SPICS_CPHA;
    if(c->mode & SPI_CPOL)
	spih->csreg |= SPICS_CPOL;
    if(c->flags & SPI_CS_HIGH) {
	/* note: SPICS_CSPOL is not effective */
	if(c->chip_sel == 0)
	    spih->csreg |= SPICS_CSPOL0;
	if(c->chip_sel == 1)
	    spih->csreg |= SPICS_CSPOL1;
    }
    if(c->flags & SPI_NO_CS)
	spih->csreg |= 3; /* no CS drive */

    /* set clock freq */
    spi_write_reg(spih, SPI_CLK, (RPI_CLOCK / c->speed) & 0xfffe);

    /* wait delay: usec to transmit 12bytes, 3/4 of FIFO size */
    spih->wait = 12LL * 8 * RPI_CLOCK / c->speed;
    return spih;
}

int spiuspace_free_bus(struct spi_handle *spih)
{
    uspace_iop_free(spih->regh);
    free(spih);
    return 0;
}

int spiuspace_start_xfer(struct spi_handle *spih)
{
    /* initialize and activate transfer */
    spi_write_reg(spih, SPI_CS, spih->csreg | SPICS_TA);

    uint8_t *rx_ptr = (uint8_t*)spih->rx_buf;
    uint8_t *tx_ptr = (uint8_t*)spih->tx_buf;
    size_t rx_resid = spih->len;
    size_t tx_resid = spih->len;

    for(;;) {
	uint32_t status = spi_read_reg(spih, SPI_CS);
	unsigned xfer_count = 0;

	if((tx_resid > 0) && (status & SPICS_TXD)) {
	    if(tx_ptr == NULL)
		spi_write_reg(spih, SPI_FIFO, 0);
	    else
		spi_write_reg(spih, SPI_FIFO, *tx_ptr++);
	    --tx_resid;
	    ++xfer_count;
	}

	if(rx_resid > 0 && (status & SPICS_RXD)) {
	    if(rx_ptr == NULL)
		spi_read_reg(spih, SPI_FIFO);
	    else
		*rx_ptr++ = spi_read_reg(spih, SPI_FIFO);
	    --rx_resid;
	    ++xfer_count;
	}

	if(tx_resid == 0 && rx_resid == 0) /* end of transfer */
	    break;

	if(xfer_count == 0) /* transfer stuck */
	    usleep(spih->wait);
    }
    return 0;
}

int spiuspace_xfer_is_done(struct spi_handle *spih)
{
    return (spi_read_reg(spih, SPI_CS) & SPICS_DONE) == 0;
}

int spiuspace_wait_xfer(struct spi_handle *spih)
{
    /* wait until transmission is done */
    while((spi_read_reg(spih, SPI_CS) & SPICS_DONE) == 0)
	usleep(spih->wait);
    return 0;
}

int spiuspace_stop_xfer(struct spi_handle *spih)
{
    /* clear TA (transfer active) bit */
    spi_write_reg(spih, SPI_CS, spih->csreg);
    return 0;
}

int spiuspace_xfer(struct spi_handle *h)
{
    return spiuspace_start_xfer(h)
	|| spiuspace_wait_xfer(h)
	|| spiuspace_stop_xfer(h);
}
