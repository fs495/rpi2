/*
 * SPI API using DMA
 *
 * ref: https://github.com/notro/spi-bcm2708/blob/master/spi-bcm2708.c
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "gpio/uspace_gpio.h"

#include "dmac/dmac.h"
#include "dmac/dmac_regs.h"

#include "spi.h"
#include "spi_regs.h"

#define RPI_CLOCK (250LL * 1000 * 1000)

/*----------------------------------------------------------------------*/

static inline
uint32_t spi_read_reg(struct spi_handle *spih, uintptr_t regoff)
{
    return *(volatile uint32_t*)(spih->regh->virt_addr + regoff);
}

static inline
void spi_write_reg(struct spi_handle *spih, uintptr_t regoff, uint32_t value)
{
    *(volatile uint32_t*)(spih->regh->virt_addr + regoff) = value;
}

static inline
size_t round_up(size_t x, size_t y)
{
    return (x + y - 1) / y * y;
}

/*----------------------------------------------------------------------*/

struct spi_handle *spidma_alloc_bus(const struct spi_config_info *c)
{
    struct spi_handle *spih = malloc(sizeof(struct spi_handle));
    if(spih == NULL)
	return NULL;

    /* setup alt func for GPIO7..GPIO11 */
    struct uspace_iop_handle *gpioh = gpio_alloc_uspace();
    if(gpioh == NULL) {
	free(spih);
	return NULL;
    }
    for(int i = 7; i <= 11; i++) {
	gpio_select_func(gpioh, i, GPFSEL_INPUT);
	gpio_select_func(gpioh, i, GPFSEL_ALT0);
    }
    uspace_iop_free(gpioh);

    /* map SPI register */
    struct uspace_iop_handle *regh = uspace_iop_alloc(SPI_OFFSET, SPI_SEGSIZE);
    if(regh == NULL) {
	free(spih);
	return NULL;
    }
    spih->regh = regh;

    /* allocate DMA stuffs */
    size_t pagesize = uspace_page_size();
    spih->tx_memh = uspace_mem_alloc(round_up(c->maxlen, pagesize));
    spih->rx_memh = uspace_mem_alloc(round_up(c->maxlen, pagesize));
    spih->cb_memh = uspace_mem_alloc(pagesize);

    dmac_init(&spih->tx_dmah);
    dmac_init(&spih->rx_dmah);
    dmac_alloc_chan(&spih->tx_dmah, c->tx_dma_chan);
    dmac_alloc_chan(&spih->rx_dmah, c->rx_dma_chan);

    /* make base SPI_CS register value */
    spih->csreg = c->chip_sel & 3; /* copy lower 2 bits only */
    if(c->mode & SPI_CPHA)
	spih->csreg |= SPICS_CPHA;
    if(c->mode & SPI_CPOL)
	spih->csreg |= SPICS_CPOL;
    if(c->flags & SPI_CS_HIGH) {
	/* note: SPICS_CSPOL is not effective */
	if(c->chip_sel == 0)
	    spih->csreg |= SPICS_CSPOL0;
	if(c->chip_sel == 1)
	    spih->csreg |= SPICS_CSPOL1;
    }
    if(c->flags & SPI_NO_CS)
	spih->csreg |= 3; /* no CS drive */
    if((spih->csreg & 3) < 2)
	spih->csreg |= SPICS_ADCS;
    spih->csreg |= SPICS_DMAEN;

    /* set clock freq */
    spi_write_reg(spih, SPI_CLK, (RPI_CLOCK / c->speed) & 0xfffe);
    spih->wait = 1000; /* TODO */

    /* bidirectional mode */
    spih->bidir = c->flags & SPI_3WIRE; /* TODO */

    return spih;
}

int spidma_free_bus(struct spi_handle *spih)
{
    dmac_free_chan(&spih->tx_dmah);
    dmac_free_chan(&spih->rx_dmah);
    dmac_fini(&spih->tx_dmah);
    dmac_fini(&spih->rx_dmah);

    uspace_mem_free(spih->tx_memh);
    uspace_mem_free(spih->rx_memh);
    uspace_mem_free(spih->cb_memh);

    uspace_iop_free(spih->regh);

    free(spih);

    return 0;
}

static void spidma_build_cbs(struct spi_handle *spih)
{
    struct dmac_ctrlblk *cb = (struct dmac_ctrlblk *)spih->cb_memh->virt_addr;

    /* data to setup flags */
    *(uint32_t*)cb = (spih->len << 16) | (spih->csreg & 0xff) | SPICS_TA;
    ++cb;

    /* from work to SPI FIFO register */
    cb->ti = (DMAC_DREQ_SPI_TX << DMACTI_PERMAP_SHIFT) | DMACTI_DEST_DREQ;
    cb->source_ad = spih->cb_memh->bus_addr;
    cb->dest_ad = spih->regh->bus_addr + SPI_FIFO;
    cb->txfr_len = 4;
    cb->stride = 0;
    cb->nextconbk = spih->cb_memh->bus_addr + 2 * sizeof(*cb);
    ++cb;
    
    /* from tx_memh to SPI_FIFO */
    cb->ti = (DMAC_DREQ_SPI_TX << DMACTI_PERMAP_SHIFT) | DMACTI_DEST_DREQ
	| DMACTI_WAIT_RESP | DMACTI_SRC_INC;
    cb->source_ad = spih->tx_memh->bus_addr;
    cb->dest_ad = spih->regh->bus_addr + SPI_FIFO;
    cb->txfr_len = spih->len;
    cb->stride = 0;
    cb->nextconbk = 0;
    ++cb;

    /* from SPI_FIFO to rx_memh */
    cb->ti = (DMAC_DREQ_SPI_RX << DMACTI_PERMAP_SHIFT) | DMACTI_SRC_DREQ
	| DMACTI_DEST_INC;
    cb->source_ad = spih->regh->bus_addr + SPI_FIFO;
    cb->dest_ad = spih->rx_memh->bus_addr;
    cb->txfr_len = spih->len;
    cb->stride = 0;
    cb->nextconbk = 0;
}

int spidma_start_xfer(struct spi_handle *spih)
{
    spi_write_reg(spih, SPI_CS, spih->csreg | SPICS_CLEAR_TX | SPICS_CLEAR_RX);
    spi_write_reg(spih, SPI_DLEN, spih->len);

    spidma_build_cbs(spih);
    dmac_start(&spih->rx_dmah, spih->cb_memh->bus_addr + 1 * sizeof(struct dmac_ctrlblk));
    dmac_start(&spih->tx_dmah, spih->cb_memh->bus_addr + 3 * sizeof(struct dmac_ctrlblk));

    return 0;
}

int spidma_xfer_is_done(struct spi_handle *spih)
{
    return dmac_is_done(&spih->tx_dmah) && dmac_is_done(&spih->rx_dmah);
}

int spidma_wait_xfer(struct spi_handle *spih)
{
    while(!spidma_xfer_is_done(spih))
	usleep(spih->wait);
    return 0;
}

int spidma_stop_xfer(struct spi_handle *spih)
{
    dmac_reset(&spih->rx_dmah);
    dmac_reset(&spih->tx_dmah);
    return 0;
}

int spidma_xfer(struct spi_handle *spih)
{
    return spidma_start_xfer(spih)
	|| spidma_wait_xfer(spih)
	|| spidma_stop_xfer(spih);
}
