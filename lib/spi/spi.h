#ifndef _spi_h_
#define _spi_h_

/*
 * common application interface to various SPI drivers
 */
#include <stdint.h>
#include <linux/spi/spidev.h>
#include "dmac/dmac.h"

struct spi_config_info {
    /* SPI clock frequency */
    uint32_t speed;

    /* flags
     *	- SPI_CS_HIGH	CS (chip select) is high-active signal
     *	- SPI_LSB_FIRST	data is LSB-first (ignored)
     *	- SPI_3WIRE	3-wire bidirectional
     *	- SPI_NO_CS	will not use CS (no automatic CS assertion)
     */
    uint32_t flags;

    /* special device file name (for spidev driver) */
    const char *dev;

    /* SPI transfer mode.
     * Specified by SPI_MODE_n directory, or by combination of
     * SPI_CPOL and/or SPI_CPHA
     */
    uint8_t mode;

    /* chip select number (for spiuspace/spidma driver)
     * 0 or 1 is valid. 2 or larger means SPI_NO_CS */
    uint8_t chip_sel;

    /* DMA channel (for spidma driver) */
    uint8_t tx_dma_chan;
    uint8_t rx_dma_chan;

    /* max buffer length (for spidma driver) */
    uint32_t maxlen;
};

struct spi_handle {
    uintptr_t tx_buf; /* for spidev/spiuspace */
    uintptr_t rx_buf; /* for spidev/spiuspace */
    size_t len; /* for all */

    /* for spidev */
    int fd;

    /* for spiuspace */
    struct uspace_iop_handle *regh;
    uint32_t csreg, wait;

    /* for spidma */
    struct uspace_mem_handle *tx_memh, *rx_memh, *cb_memh;
    struct dmac_handle tx_dmah, rx_dmah;
    uint8_t bidir;
};

/*----------------------------------------------------------------------*/

struct spi_handle *spidev_alloc_bus(const struct spi_config_info *c);
int spidev_free_bus(struct spi_handle *spih);
int spidev_xfer(struct spi_handle *spih);

struct spi_handle *spiuspace_alloc_bus(const struct spi_config_info *c);
int spiuspace_free_bus(struct spi_handle *spih);
int spiuspace_xfer_is_done(struct spi_handle *spih);
int spiuspace_wait_xfer(struct spi_handle *spih);
int spiuspace_stop_xfer(struct spi_handle *spih);
int spiuspace_xfer(struct spi_handle *spih);

/*
 * NOTE!NOTE!NOTE!NOTE!NOTE!NOTE!NOTE!NOTE!
 * spidma is highly experimental and unstable
 */
struct spi_handle *spidma_alloc_bus(const struct spi_config_info *c);
int spidma_free_bus(struct spi_handle *spih);
int spidma_xfer_is_done(struct spi_handle *spih);
int spidma_wait_xfer(struct spi_handle *spih);
int spidma_stop_xfer(struct spi_handle *spih);
int spidma_xfer(struct spi_handle *spih);

#endif /* _spi_h_ */
