/*
 * DMA test (memory-to-memory copy without CPU)
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include "dmac/dmac.h"

#define DMA_CHAN 5

static void mem_dump(void *ptr, int len)
{
    int i;
    uint8_t *p = (uint8_t *)ptr;

    for(i = 0; i < len; i++) {
	if(i % 16 == 0)
	    printf("0x%04x ", i);
	printf("%02x ", p[i]);
	if(i % 16 == 15)
	    printf("\n");
    }
    printf("\n");
}

static void show_memmap(void)
{
    pid_t pid = getpid();
    char buf[256];
    sprintf(buf, "cat /proc/%d/maps", pid);
    system(buf);
}

int main(int argc, char *argv[])
{
    /* DMA handle */
    struct dmac_handle dmah;
    dmac_init(&dmah);
    dmac_alloc_chan(&dmah, DMA_CHAN);
    uspace_iop_print(dmah.ioph);

    /* memory for control block, source/dest buffer */
    struct uspace_mem_handle *cb_memh = uspace_mem_alloc(sizeof(struct dmac_ctrlblk));
    struct uspace_mem_handle *src_memh = uspace_mem_alloc(256);
    struct uspace_mem_handle *dst_memh = uspace_mem_alloc(256);
    uspace_mem_print(cb_memh);
    uspace_mem_print(src_memh);
    uspace_mem_print(dst_memh);
    show_memmap();

    /* setup buffer */
    for(int i = 0; i < 256; i++) {
	*(uint8_t*)(src_memh->virt_addr + i) = i;
	*(uint8_t*)(dst_memh->virt_addr + i) = 0;
    }

    /* make up DMA control block */
    struct dmac_ctrlblk *cb = cb_memh->virt_addr;
    memset(cb, 0, sizeof(*cb));
    cb->ti = DMACTI_SRC_INC | DMACTI_DEST_INC;
    cb->source_ad = src_memh->bus_addr;
    cb->dest_ad = dst_memh->bus_addr;
    cb->txfr_len = 256;
    cb->stride = 0;
    cb->nextconbk = 0;

    /* start DMA operation */
    printf("\nStarting DMA\n");
    dmac_start(&dmah, cb_memh->bus_addr);
    dmac_print(&dmah);

    /* wait for completion */
    while(!dmac_is_done(&dmah)) {
	printf("Waiting for end of DMA: CS\t=%08x\n", dmac_read_chan_reg(&dmah, DMAC_CS));
	usleep(100);
    }
    dmac_reset(&dmah);

    /* show result */
    mem_dump(dst_memh->virt_addr, 256);
    for(int i = 0; i < 256; i++) {
	uint8_t val = *(uint8_t*)(dst_memh->virt_addr + i);
	if(val != i)
	    printf("Error: diff at %i, value 0x%0x\n", i, val);
    }

    printf("test done\n");
    return 0;
}
