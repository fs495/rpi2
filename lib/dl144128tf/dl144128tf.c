#include <sys/time.h>
#include <unistd.h>

#include "dl144128tf.h"
#include "../dl144128tf/ili9163c.h"

void dl144128tf_write(struct dl144128tf *lcd, uint8_t cmd,
		      const uint8_t *param, size_t paramlen)
{
    struct spi_handle *spih = lcd->spih;
    struct uspace_iop_handle *gpioh = lcd->gpioh;

    gpio_clr(gpioh, lcd->pin.csx);
    gpio_clr(gpioh, lcd->pin.dcx);

    /* send command byte */
    spih->tx_buf = (uintptr_t)&cmd;
    spih->rx_buf = 0;
    spih->len = 1;
    lcd->spi_xfer(spih);

    /* send paramater bytes if it exists */
    if(param != NULL && paramlen > 0) {
	gpio_set(gpioh, lcd->pin.dcx);
	spih->tx_buf = (uintptr_t)param;
	spih->rx_buf = 0;
	spih->len = paramlen;
	lcd->spi_xfer(spih);
    }

    gpio_set(gpioh, lcd->pin.csx);
}


struct dl144128tf *dl144128tf_init(const struct dl144128tf_config *config)
{
    unsigned char par[6];

    struct dl144128tf *lcd = malloc(sizeof(struct dl144128tf));
    if(lcd == NULL)
	return NULL;

    /* initialize */
    switch(config->spi_driver) {
    case SPI_SPIDEV:
	lcd->spi_xfer = spidev_xfer;
	lcd->spih = spidev_alloc_bus(&config->spi_config);
	break;
    case SPI_USPACE:
	lcd->spi_xfer = spiuspace_xfer;
	lcd->spih = spiuspace_alloc_bus(&config->spi_config);
	break;
    default:
	free(lcd);
	return NULL;
    }
    lcd->spih->tx_buf = (uintptr_t)0;
    lcd->spih->rx_buf = (uintptr_t)0;

    lcd->gpioh = gpio_alloc_uspace();
    lcd->pin = config->pin_config;
    gpio_select_func(lcd->gpioh, lcd->pin.resx, GPFSEL_OUTPUT);
    gpio_select_func(lcd->gpioh, lcd->pin.dcx, GPFSEL_OUTPUT);
    gpio_select_func(lcd->gpioh, lcd->pin.csx, GPFSEL_OUTPUT);

    /* hardware reset */
    gpio_set(lcd->gpioh, lcd->pin.csx);
    usleep(1000);
    gpio_clr(lcd->gpioh, lcd->pin.resx);
    usleep(10000);
    gpio_set(lcd->gpioh, lcd->pin.resx);
    usleep(50000);

    /* Software Reset */
    dl144128tf_write(lcd, ILI9163C_SWRESET, NULL, 0);
    usleep(1000);

    /* Sleep Out */
    dl144128tf_write(lcd, ILI9163C_SLPOUT, NULL, 0);
    usleep(20000);

    /* Set default gamma */
    par[0] = 4;
    dl144128tf_write(lcd, ILI9163C_GAMMASET, par, 1);

    par[0] = 0;
    dl144128tf_write(lcd, ILI9163C_GAMRSEL, par, 1);

    par[0] = 0x0a; par[1] = 0x14;
    dl144128tf_write(lcd, ILI9163C_FRMCTR1, par, 2);

#if 0
    par[0] = 0x0d; par[1] = 05;
    dl144128tf_write(lcd, ILI9163C_PWCTR1, par, 2);

    par[0] = 2;
    dl144128tf_write(lcd, ILI9163C_PWCTR2, par, 1);

    par[0] = 0x35; par[1] = 0x37;
    dl144128tf_write(lcd, ILI9163C_VCOMCTR1, par, 2);

    par[0] = 0x40;
    dl144128tf_write(lcd, ILI9163C_VCOMOFFS, par, 1);
#endif

    /* column address set */
    par[0] = 0; par[1] = 0; par[2] = 0; par[3] = 127;
    dl144128tf_write(lcd, ILI9163C_CLMADRS, par, 4);

    /* page address set */
    par[3] = 159;
    dl144128tf_write(lcd, ILI9163C_PGEADRS, par, 4);

    /* Memory Access Control */
    par[0] = 0xc0;
    dl144128tf_write(lcd, ILI9163C_MADCTL, par, 1);

    /* Set Color Format */
    par[0] = 6;
    dl144128tf_write(lcd, ILI9163C_PIXFMT, par, 1);

    /* Set Vertical Scroll Area */
    par[0] = par[1] = 0;
    par[2] = 0; par[3] = 128;
    par[4] = par[5] = 0;
    dl144128tf_write(lcd, ILI9163C_VSCLLDEF, par, 6);

    /* Display On */
    dl144128tf_write(lcd, ILI9163C_DISPON, NULL, 0);

    return lcd;
}

void dl144128tf_sync(struct dl144128tf *lcd, const void *vram)
{
    dl144128tf_write(lcd, ILI9163C_RAMWR, vram, 128 * 128 * 3);
}

