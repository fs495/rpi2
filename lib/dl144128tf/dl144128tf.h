#ifndef _dl144128tf_h_
#define _dl144128tf_h_

#include <stdint.h>
#include "gpio/uspace_gpio.h"
#include "spi/spi.h"
#include "dl144128tf/ili9163c.h"

struct dl144128tf_config {
    /* GPIO numbers of each signal (not pin number) */
    struct pin_config {
	uint8_t csx;
	uint8_t dcx;
	uint8_t resx;
    } pin_config;

    enum SPI_DRIVER {
	SPI_INVALID,
	SPI_SPIDEV,
	SPI_USPACE,
    } spi_driver;

    /* SPI configurations */
    struct spi_config_info spi_config;
};

struct dl144128tf {
    struct uspace_iop_handle *gpioh;
    struct spi_handle *spih;
    struct pin_config pin;
    int (*spi_xfer)(struct spi_handle *spih);
};

/*====================================================================== */

struct dl144128tf *dl144128tf_init(const struct dl144128tf_config *config);

void dl144128tf_write(struct dl144128tf *lcd, uint8_t cmd,
		      const uint8_t *param, size_t paramlen);

void dl144128tf_sync(struct dl144128tf *lcd, const void *vram);

#endif /* _dl144128tf_h_ */
