#include <unistd.h>
#include "dl144128tf/dl144128tf.h"
#include "graphics/graphics.h"

static const struct dl144128tf_config myconfig = {
    .pin_config = {
	.csx = 8,
	.dcx = 25,
	.resx = 24,
    },

    .spi_driver = SPI_SPIDEV,

    .spi_config = {
	.mode = SPI_MODE_0,
	.speed = 50 * 1000 * 1000, /* 50MHz */
	.flags = SPI_3WIRE | SPI_NO_CS,
	.dev = "/dev/spidev0.0",
	.chip_sel = 3, /* no auto CS assertion */
    },
};

unsigned char vram[128 * 3 * 128];

int main(int argc, char **argv)
{
    struct dl144128tf *lcd = dl144128tf_init(&myconfig);
    g_setBaseAddr(vram);

    int i, ix, iy;

#if 1
    /*----------------------------------------------------------------------*/
    g_fillScreen(G_BLACK);
    for(i = 0; i <= 128; i += 8)
	g_drawLine(0, 0, 127, i, G_YELLOW);
    for(i = 0; i <= 128; i += 8)
	g_drawLine(0, 0, i, 127, G_YELLOW);
    dl144128tf_sync(lcd, vram);
    sleep(1);
#endif

#if 1
    /*----------------------------------------------------------------------*/
    g_fillScreen(G_BLACK);
    for(i = 4; i < 64; i += 4)
	g_drawRect(64 - i, 64 - i, i*2, i*2, G_GREEN);
    dl144128tf_sync(lcd, vram);
    sleep(1);
#endif

#if 1
    /*----------------------------------------------------------------------*/
    g_fillScreen(G_BLACK);
    for(i = 4; i < 64; i += 4)
	g_drawCircle(64, 64, i, G_RED);
    dl144128tf_sync(lcd, vram);
    sleep(1);
#endif

    /*----------------------------------------------------------------------*/
#if 1
    g_fillScreen(G_BLACK);
    for(ix = 16; ix < 128; ix += 16)
	for(iy = 16; iy < 128; iy += 16)
	    g_fillCircle(ix, iy, 8, G_CYAN);
    for(ix = 24; ix < 120; ix += 16)
	for(iy = 24; iy < 120; iy += 16)
	    g_drawCircle(ix, iy, 8, G_BLUE);
    dl144128tf_sync(lcd, vram);
    sleep(1);
#endif

#if 1
    g_fillScreen(G_BLACK);
    for(i = 0; i < 256; i += 2) {
	/* RGB */
	g_drawFastHLine(0x00, i/2, 7, g_mkcolor(0, 0, i));
	g_drawFastHLine(0x08, i/2, 7, g_mkcolor(0, 0, 255-i));
	g_drawFastHLine(0x10, i/2, 7, g_mkcolor(0, i, 0));
	g_drawFastHLine(0x18, i/2, 7, g_mkcolor(0, 255-i, 0));
	g_drawFastHLine(0x20, i/2, 7, g_mkcolor(i, 0, 0));
	g_drawFastHLine(0x28, i/2, 7, g_mkcolor(255-i, 0, 0));
	/* CMY */
	g_drawFastHLine(0x30, i/2, 7, g_mkcolor(i, i, 0));
	g_drawFastHLine(0x38, i/2, 7, g_mkcolor(255-i, 255-i, 0));
	g_drawFastHLine(0x40, i/2, 7, g_mkcolor(i, 0, i));
	g_drawFastHLine(0x48, i/2, 7, g_mkcolor(255-i, 0, 255-i));
	g_drawFastHLine(0x50, i/2, 7, g_mkcolor(0, i, i));
	g_drawFastHLine(0x58, i/2, 7, g_mkcolor(0, 255-i, 255-i));
	/* B&W */
	g_drawFastHLine(0x60, i/2, 7, g_mkcolor(i, i, i));
	g_drawFastHLine(0x68, i/2, 7, g_mkcolor(255-i, 255-i, 255-i));
    }
    /* color bar */
    for(i = 0; i < 128; i++) {
	if(i >= 0 && i < 16)
	    g_drawFastHLine(0x70, i, 15, g_mkcolor(240, 0, 16*(i-0)));
	else if(i >= 16 && i < 32)
	    g_drawFastHLine(0x70, i, 15, g_mkcolor(240-16*(i-16), 0, 240));
	else if(i >= 32 && i < 48)
	    g_drawFastHLine(0x70, i, 15, g_mkcolor(0, 16*(i-32), 240));
	else if(i >= 48 && i < 64)
	    g_drawFastHLine(0x70, i, 15, g_mkcolor(0, 240, 240-16*(i-48)));
	else if(i >= 64 && i < 80)
	    g_drawFastHLine(0x70, i, 15, g_mkcolor(16*(i-64), 240, 0));
	else if(i >= 80 && i < 96)
	    g_drawFastHLine(0x70, i, 15, g_mkcolor(240, 240-16*(i-80), 0));
    }
    dl144128tf_sync(lcd, vram);
    sleep(1);
#endif

    g_fillScreen(G_BLACK);
    dl144128tf_sync(lcd, vram);

    return 0;
}
