#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "mailbox.h"
#include "uspace.h"

/*----------------------------------------------------------------------*/

static size_t page_size = 0;
static uintptr_t iop_phys_base_addr = 0;
static unsigned mbox_mem_flag = 0;

/**
 * ページサイズを取得する
 */
long uspace_page_size(void)
{
    if(page_size == 0)
	page_size = sysconf(_SC_PAGE_SIZE);
    return page_size;
}

/*
 * Raspberry Piのモデルを判別し、モデル固有情報を保存する
 */
static int uspace_detect_hardware_model(void)
{
    if(iop_phys_base_addr && mbox_mem_flag)
	return 0; /* すでに判別済み */

    FILE *fp = fopen("/proc/cpuinfo", "r");
    if(fp == NULL) {
	fprintf(stderr, "Error: cannot open /proc/cpuinfo\n");
	return -1;
    }

    char buf[256];
    while(fgets(buf, sizeof(buf), fp) != NULL) {
	if(strncmp(buf, "Hardware", 8) == 0) {
	    if(strstr(buf, "BCM2708") != NULL) {
		iop_phys_base_addr = IOP_PHYS_BASE_BCM2708;
		mbox_mem_flag = MEM_FLAG_BMC2708;
	    } else if(strstr(buf, "BCM2709") != NULL) {
		iop_phys_base_addr = IOP_PHYS_BASE_BCM2709;
		mbox_mem_flag = MEM_FLAG_BMC2709;
	    } /* add newer model here */
	}
    }
    fclose(fp);

    if(iop_phys_base_addr == 0) {
	fprintf(stderr, "Error: cannot identify hardware\n");
	return -1;
    }

    return 0;
}


/*----------------------------------------------------------------------*/

/**
 * 物理メモリ領域を仮想メモリ空間にマップする
 */
void *uspace_map(uintptr_t phys_addr, size_t size)
{
    int memfd = open("/dev/mem", O_RDWR | O_SYNC);
    if(memfd < 0) {
	perror("/dev/mem");
	fprintf(stderr, "Error: uspace_map: cannot open /dev/mem\n");
	return NULL;
    }

    void *virt_addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED,
			   memfd, phys_addr);
    close(memfd);

    if(virt_addr == MAP_FAILED) {
	perror(NULL);
	fprintf(stderr, "Error: uspace_map: phys_addr=0x%x size=0x%x\n",
		phys_addr, size);
	return NULL;
    }
    return virt_addr;
}

/*
 * 割り当てた仮想メモリ空間を解放する
 */
void uspace_unmap(void *virt_addr, size_t size)
{
    if(munmap(virt_addr, size) < 0) {
	perror(NULL);
	fprintf(stderr, "Error: uspace_unmap: cannot munmap\n");
    }
    return;
}

/*----------------------------------------------------------------------*/

/*
 * 周辺IOをユーザ仮想空間に割りつける
 *
 * @iop_offset	周辺IO空間内でのオフセット
 * @size	割りつけるサイズ
 */
struct uspace_iop_handle *uspace_iop_alloc(size_t iop_offset, size_t size)
{
    struct uspace_iop_handle *ioph = malloc(sizeof(struct uspace_iop_handle));
    if(ioph == NULL)
	return NULL;

    if(uspace_detect_hardware_model())
	return 0;

    ioph->phys_base_addr = iop_phys_base_addr;
    ioph->phys_addr = ioph->phys_base_addr + iop_offset;
    ioph->bus_addr = IOP_BUS_BASE + iop_offset;
    ioph->virt_addr = uspace_map(ioph->phys_addr, size);
    ioph->size = size;
    return ioph;
}

/*
 * 周辺IOを解放する
 */
void uspace_iop_free(struct uspace_iop_handle *ioph)
{
    uspace_unmap(ioph->virt_addr, ioph->size);
    memset(ioph, 0, sizeof(struct uspace_iop_handle));
    free(ioph);
}

void uspace_iop_print(struct uspace_iop_handle *ioph)
{
    printf("iop: virt=%p phys=%x bus=%x\n",
	   ioph->virt_addr, ioph->phys_addr, ioph->bus_addr);
}

/*----------------------------------------------------------------------*/

/*
 * VideoCoreのmailbox APIを使い、ロックダウンされたメモリ領域を割り当てる
 */
struct uspace_mem_handle *uspace_mem_alloc(size_t size)
{
    if(uspace_detect_hardware_model())
	return NULL;
    if(uspace_page_size() == 0)
	return NULL;

    struct uspace_mem_handle *memh = malloc(sizeof(struct uspace_mem_handle));
    if(memh == NULL)
	return NULL;

    memh->handle = mbox_open();
    memh->alloc_size = (size + page_size - 1) / page_size * page_size;
    memh->mem_ref = mbox_mem_alloc(memh->handle, memh->alloc_size, page_size,
				   mbox_mem_flag);
    memh->bus_addr = mbox_mem_lock(memh->handle, memh->mem_ref);

    memh->phys_addr = memh->bus_addr & ~0xc0000000;
    memh->virt_addr = uspace_map(memh->phys_addr, memh->alloc_size);
    return memh;
}

/*
 * ロックダウンされたメモリを解放する
 */
void uspace_mem_free(struct uspace_mem_handle *memh)
{
    uspace_unmap(memh->virt_addr, memh->alloc_size);
    mbox_mem_unlock(memh->handle, memh->alloc_size);
    mbox_mem_free(memh->handle, memh->mem_ref);
    free(memh);
}

void uspace_mem_print(struct uspace_mem_handle *memh)
{
    printf("mem: virt=%p phys=%x bus=%x\n",
	   memh->virt_addr, memh->phys_addr, memh->bus_addr);
}
