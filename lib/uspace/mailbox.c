/*
Copyright (c) 2012, Broadcom Europe Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the copyright holder nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>

#include "mailbox.h"

#define MAJOR_NUM 100
#define IOCTL_MBOX_PROPERTY _IOWR(MAJOR_NUM, 0, char *)
#define DEVICE_FILE_NAME "/dev/vcio"

/*
 * use ioctl to send mbox property message
 */

static int mbox_property(int mboxfd, void *buf)
{
    int ret_val = ioctl(mboxfd, IOCTL_MBOX_PROPERTY, buf);

    if (ret_val < 0) {
	printf("ioctl_set_msg failed:%d\n", ret_val);
    }

#ifdef DEBUG
    unsigned *p = buf; int i; unsigned size = *(unsigned *)buf;
    for (i=0; i<size/4; i++)
	printf("%04x: 0x%08x\n", i*sizeof *p, p[i]);
#endif
    return ret_val;
}

unsigned mbox_mem_alloc(int mboxfd, unsigned size, unsigned align, unsigned flags)
{
    int i=0;
    unsigned p[32];
    p[i++] = 0; // size
    p[i++] = 0x00000000; // process request

    p[i++] = 0x3000c; // (the tag id)
    p[i++] = 12; // (size of the buffer)
    p[i++] = 12; // (size of the data)
    p[i++] = size; // (num bytes? or pages?)
    p[i++] = align; // (alignment)
    p[i++] = flags; // (MEM_FLAG_L1_NONALLOCATING)

    p[i++] = 0x00000000; // end tag
    p[0] = i*sizeof *p; // actual size

    mbox_property(mboxfd, p);
    return p[5];
}

unsigned mbox_mem_free(int mboxfd, unsigned handle)
{
    int i=0;
    unsigned p[32];
    p[i++] = 0; // size
    p[i++] = 0x00000000; // process request

    p[i++] = 0x3000f; // (the tag id)
    p[i++] = 4; // (size of the buffer)
    p[i++] = 4; // (size of the data)
    p[i++] = handle;

    p[i++] = 0x00000000; // end tag
    p[0] = i*sizeof *p; // actual size

    mbox_property(mboxfd, p);
    return p[5];
}

unsigned mbox_mem_lock(int mboxfd, unsigned handle)
{
    int i=0;
    unsigned p[32];
    p[i++] = 0; // size
    p[i++] = 0x00000000; // process request

    p[i++] = 0x3000d; // (the tag id)
    p[i++] = 4; // (size of the buffer)
    p[i++] = 4; // (size of the data)
    p[i++] = handle;

    p[i++] = 0x00000000; // end tag
    p[0] = i*sizeof *p; // actual size

    mbox_property(mboxfd, p);
    return p[5];
}

unsigned mbox_mem_unlock(int mboxfd, unsigned handle)
{
    int i=0;
    unsigned p[32];
    p[i++] = 0; // size
    p[i++] = 0x00000000; // process request

    p[i++] = 0x3000e; // (the tag id)
    p[i++] = 4; // (size of the buffer)
    p[i++] = 4; // (size of the data)
    p[i++] = handle;

    p[i++] = 0x00000000; // end tag
    p[0] = i*sizeof *p; // actual size

    mbox_property(mboxfd, p);
    return p[5];
}

int mbox_open(void) {
    int mboxfd;

    // open a char device file used for communicating with kernel mbox driver
    mboxfd = open(DEVICE_FILE_NAME, 0);
    if (mboxfd < 0) {
	fprintf(stderr, "Can't open device file: %s\n", DEVICE_FILE_NAME);
	fprintf(stderr, "Try creating a device file with: sudo mknod %s c %d 0\n", DEVICE_FILE_NAME, MAJOR_NUM);
	exit(-1);
    }
    return mboxfd;
}

void mbox_close(int mboxfd) {
    close(mboxfd);
}
