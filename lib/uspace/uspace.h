#ifndef _uspace_h_
#define _uspace_h_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

long uspace_page_size(void);

/*----------------------------------------------------------------------
 * 物理アドレスとバスアドレス
 */
#define IOP_BUS_BASE 0x7e000000

#define IOP_PHYS_BASE_BCM2708 0x20000000 /* ARMv6: A, A+, B, B+ */
#define IOP_PHYS_BASE_BCM2709 0x3f000000 /* ARMv7: 2 model B */

#define MEM_FLAG_BMC2708 0x0c
#define MEM_FLAG_BMC2709 0x04

/*----------------------------------------------------------------------
 * 物理アドレスで指定された領域の仮想アドレスアドレス空間へのマップ
 */
void *uspace_map(uintptr_t phys_addr, size_t size);
void uspace_unmap(void *virt_addr, size_t size);

/*----------------------------------------------------------------------
 * 周辺IO
 *
 * 周辺IO領域(バスアドレス0x7e00_0000〜)内でのオフセットで指定する。
 * マニュアルに記載があるのはバスアドレスなので、直接は指定できない。
 * 例: GPIOはバスアドレス0x7e20_0000なので、オフセット0x0020_0000を指定。
 *
 * ※mmapやuspace_mapは物理アドレスを必要とするが、
 * Raspberry PiではIO領域の物理アドレスはモデルによって異なる。
 */
struct uspace_iop_handle {
    void *virt_addr;
    uintptr_t phys_addr;
    uintptr_t bus_addr;

    uintptr_t phys_base_addr;
    size_t size;
};

struct uspace_iop_handle *uspace_iop_alloc(size_t iop_offset, size_t size);
void uspace_iop_free(struct uspace_iop_handle *ioph);
void uspace_iop_print(struct uspace_iop_handle *ioph);

/*----------------------------------------------------------------------
 * メモリ
 */
struct uspace_mem_handle {
    void *virt_addr;
    uintptr_t phys_addr;
    uintptr_t bus_addr;

    int handle;
    size_t alloc_size;
    unsigned mem_ref;
};

struct uspace_mem_handle *uspace_mem_alloc(size_t size);
void uspace_mem_free(struct uspace_mem_handle *memh);
void uspace_mem_print(struct uspace_mem_handle *memh);

#endif /* _uspace_h_ */
