#ifndef __gpio_regs_h__
#define __gpio_regs_h__

/*
 * GPIO register offsets
 * - based on BCM2836 datasheet
 * - not sure these can be applied to other models
 */
enum gpio_reg_offset {
    GPFSEL0 = 0x00,	/* GPIO Function Select 0 32 R/W */
    GPFSEL1 = 0x04,	/* GPIO Function Select 1 32 R/W */
    GPFSEL2 = 0x08,	/* GPIO Function Select 2 32 R/W */
    GPFSEL3 = 0x0c,	/* GPIO Function Select 3 32 R/W */
    GPFSEL4 = 0x10,	/* GPIO Function Select 4 32 R/W */
    GPFSEL5 = 0x14,	/* GPIO Function Select 5 32 R/W */
    GPSET0 = 0x1c,	/* GPIO Pin Output Set 0 32 W */
    GPSET1 = 0x20,	/* GPIO Pin Output Set 1 32 W */
    GPCLR0 = 0x28,	/* GPIO Pin Output Clear 0 32 W */
    GPCLR1 = 0x2c,	/* GPIO Pin Output Clear 1 32 W */
    GPLEV0 = 0x34,	/* GPIO Pin Level 0 32 R */
    GPLEV1 = 0x38,	/* GPIO Pin Level 1 32 R */
    GPEDS0 = 0x40,	/* GPIO Pin Event Detect Status 0 32 R/W */
    GPEDS1 = 0x44,	/* GPIO Pin Event Detect Status 1 32 R/W */
    GPREN0 = 0x4c,	/* GPIO Pin Rising Edge Detect Enable 0 32 R/W */
    GPREN1 = 0x50,	/* GPIO Pin Rising Edge Detect Enable 1 32 R/W */
    GPFEN0 = 0x58,	/* GPIO Pin Falling Edge Detect Enable 0 32 R/W */
    GPFEN1 = 0x5c,	/* GPIO Pin Falling Edge Detect Enable 1 32 R/W */
    GPHEN0 = 0x64,	/* GPIO Pin High Detect Enable 0 32 R/W */
    GPHEN1 = 0x68,	/* GPIO Pin High Detect Enable 1 32 R/W */
    GPLEN0 = 0x70,	/* GPIO Pin Low Detect Enable 0 32 R/W */
    GPLEN1 = 0x74,	/* GPIO Pin Low Detect Enable 1 32 R/W */
    GPAREN0 = 0x7c,	/* GPIO Pin Async. Rising Edge Detect 0 32 R/W */
    GPAREN1 = 0x80,	/* GPIO Pin Async. Rising Edge Detect 1 32 R/W */
    GPAFEN0 = 0x88,	/* GPIO Pin Async. Falling Edge Detect 0 32 R/W */
    GPAFEN1 = 0x8c,	/* GPIO Pin Async. Falling Edge Detect 1 32 R/W */
    GPPUD = 0x94,	/* GPIO Pin Pull-up/down Enable 32 R/W */
    GPPUDCLK0 = 0x98,	/* GPIO Pin Pull-up/down Enable Clock 0 32 R/W */
    GPPUDCLK1 = 0x9c,	/* GPIO Pin Pull-up/down Enable Clock 1 32 R/W */
};

/* GPFSELn register value */
enum gpio_function {
    GPFSEL_INPUT = 0,
    GPFSEL_OUTPUT = 1,
    GPFSEL_ALT0 = 4,
    GPFSEL_ALT1 = 5,
    GPFSEL_ALT2 = 6,
    GPFSEL_ALT3 = 7,
    GPFSEL_ALT4 = 3,
    GPFSEL_ALT5 = 2,
};

#define GPIO_OFFSET 0x00200000	/* offset from I/O peripheral to GPIO */
#define GPIO_SEGSIZE 0xb4	/* mmap size */

#endif /* __gpio_regs_h__ */
