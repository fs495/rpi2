#ifndef _gpio_ureg_h
#define _gpio_ureg_h

#include "gpio_regs.h"
#include "uspace/uspace.h"

/*======================================================================
 * functions (prototype and inline)
 */
struct uspace_iop_handle *gpio_alloc_uspace(void);

void gpio_select_func(struct uspace_iop_handle *ioph,
		      size_t pin, size_t func);

static inline
volatile uint32_t *gpio_reg_addr(struct uspace_iop_handle *ioph,
				 size_t offset)
{
    return (volatile uint32_t*)(ioph->virt_addr + offset);
}

static inline
volatile uint32_t *gpio_reg_idxaddr(struct uspace_iop_handle *ioph,
				    size_t offset, size_t idx)
{
    return (volatile uint32_t*)(ioph->virt_addr + offset + idx * 4);
}

static inline
void gpio_write_reg(struct uspace_iop_handle *ioph, size_t offset, uint32_t val)
{
    *(volatile uint32_t*)(ioph->virt_addr + offset) = val;
}

static inline
uint32_t gpio_read_reg(struct uspace_iop_handle *ioph, size_t offset)
{
    return *(volatile uint32_t*)(ioph->virt_addr + offset);
}

static inline
void gpio_set(struct uspace_iop_handle *ioph, unsigned pin)
{
    gpio_write_reg(ioph, GPSET0 + (pin / 32) * 4, 1 << (pin % 32));
}

static inline
void gpio_clr(struct uspace_iop_handle *ioph, unsigned pin)
{
    gpio_write_reg(ioph, GPCLR0 + (pin / 32) * 4, 1 << (pin % 32));
}

static inline
unsigned gpio_get(struct uspace_iop_handle *ioph, unsigned pin)
{
    return gpio_read_reg(ioph, GPLEV0 + (pin / 32) * 4) >> (pin % 32) & 1;
}

#endif /* _gpio_ureg_h */
