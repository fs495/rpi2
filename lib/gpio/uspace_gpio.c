#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "uspace_gpio.h"

struct uspace_iop_handle *gpio_alloc_uspace(void)
{
    return uspace_iop_alloc(GPIO_OFFSET, GPIO_SEGSIZE);
}

void gpio_select_func(struct uspace_iop_handle *ioph,
		      unsigned pin, unsigned func)
{
    unsigned index = pin / 10;
    unsigned pos = (pin % 10) * 3;
    volatile uint32_t *ptr = gpio_reg_idxaddr(ioph, GPFSEL0, index);
    *ptr = (*ptr & ~(7 << pos)) | (func << pos);
}
