#ifndef __dmac_regs_h__
#define __dmac_regs_h__

/*======================================================================
 * DMA controller constants
 */

/*
 * DMAC register offsets
 */
enum dmac_reg_offset {
    /* Each channel has DMAC_CS ~ DMAC_DEBUG register */
    DMAC_CS = 0,
    DMAC_CONBLK_AD = 4,
    DMAC_TI = 8,
    DMAC_SOURCE_AD = 12,
    DMAC_DEST_AD = 16,
    DMAC_TXFR_LEN = 20,
    DMAC_STRIDE = 24,
    DMAC_NEXTCONBK = 28,
    DMAC_DEBUG = 32,
    DMAC_INT_STATUS = 0xfe0, /* full version only */
    DMAC_ENABLE = 0xff0, /* full version only */
};

enum dmac_cs_value {
    DMACCS_RESET = 1 << 31,
    DMACCS_ABORT = 1 << 30,
    DMACCS_DISDEBUG = 1 << 29,
    DMACCS_WAIT_FOR_OUTSTAND_WRITE = 1 << 28,
    DMACCS_PANIC_PRIORITY = 0xf00000,
    DMACCS_PRIORITY       = 0x0f0000,
    DMACCS_ERROR = 1 << 8,
    DMACCS_WAIITING_FOR_OUTTAND_WRITE= 1 << 6,
    DMACCS_DREQ_STOPS_DMA = 1 << 5,
    DMACCS_PAUSED = 1 << 4,
    DMACCS_DREQ = 1 << 3,
    DMACCS_INT = 1 << 2,
    DMACCS_END = 1 << 1,
    DMACCS_ACTIVE = 1 << 0,
};

enum dmac_ti_value {
    DMACTI_NO_WIDE_BURSTS = 1 << 26, /* full version only */
    DMACTI_WAIT_SHIFT = 21,
    DMACTI_WAITS = 31 << DMACTI_WAIT_SHIFT,
    DMACTI_PERMAP_SHIFT = 16,
    DMACTI_PERMAP = 31 << DMACTI_PERMAP_SHIFT,
    DMACTI_BURST_LENGTH_SHIFT = 12,
    DMACTI_BURST_LENGTH = 15 << DMACTI_BURST_LENGTH_SHIFT,

    DMACTI_SRC_IGNORE = 1 << 11, /* full version only */
    DMACTI_SRC_DREQ = 1 << 10,
    DMACTI_SRC_WIDTH = 1 << 9,
    DMACTI_SRC_INC = 1 << 8,
    
    DMACTI_DEST_IGNORE = 1 << 7, /* full version only */
    DMACTI_DEST_DREQ = 1 << 6,
    DMACTI_DEST_WIDTH = 1 << 5,
    DMACTI_DEST_INC = 1 << 4,

    DMACTI_WAIT_RESP = 1 << 3,
    DMACTI_TDMODE = 1 << 1, /* full version only */
    DMACTI_INTEN = 1 << 0,
};

enum dmac_xfer_len_value {
    DMACXL_YLEN_SHIFT = 16, /* full version only */
    DMACXL_YLEN = 0x3fff << DMACXL_YLEN_SHIFT, /* full version only */
    DMACXL_XLEN = 0xffff,
};

enum dmac_stride_len_value { /* full version only */
    DMACSTRIDE_D_SHIFT = 16,
    DMACSTRIDE_D = 0xffff << DMACSTRIDE_D_SHIFT,
    DMACSTRIDE_S = 0xffff,
};

enum dmac_debug_value {
    DMACDBG_LITE = 1 << 28,
    DMACDBG_VERSION = 7 << 25,
    DMACDBG_STATE = 0x1ff << 16,
    DMACDBG_DMA_ID = 0xff << 8,
    DMACDBG_OUTSTAND_WRITE = 0xf << 4,
    DMACDBG_READ_ERROR = 1 << 2,
    DMACDBG_FIFO_ERROR = 1 << 1,
    DMACDBG_READ_LAST_NOT_SET_ERROR = 1 << 0,
};

enum dmac_dreq_value {
    DMAC_DREQ_ALWAYS = 0,
    DMAC_DREQ_PCM_TX = 2,
    DMAC_DREQ_PCM_RX = 3,
    DMAC_DREQ_PWM = 5,
    DMAC_DREQ_SPI_TX = 6,
    DMAC_DREQ_SPI_RX = 7,
    DMAC_DREQ_BSC_TX = 8,
    DMAC_DREQ_BSC_RX = 9,
    DMAC_DREQ_UART_TX = 12,
    DMAC_DREQ_UART_RX = 14,
};

#define DMAC_OFFSET 0x00007000	/* offset from I/O peripheral to DMAC */
#define DMAC_SEGSIZE 0x0f00	/* mmap size */

struct dmac_ctrlblk {
    uint32_t ti;
    uint32_t source_ad;
    uint32_t dest_ad;
    uint32_t txfr_len;
    uint32_t stride;
    uint32_t nextconbk;
    uint32_t reserved1;
    uint32_t reserved2;
};

#endif /* __dmac_regs_h__ */
