#ifndef _dmac_h_
#define _dmac_h_

/*
 * BCM2835 DMA Controller
 *
 * - there are channel 0-15, this header covers 0-14 only
 *   (channel 15 is at special location)
 * - channel 0-6 are full version, 7-14(+15) are LITE version
 *
 * DMA channel consideration:
 * https://pibrewstuff.wordpress.com/2015/01/13/raspberry-pi-dma-channels/
 * According to this page, 4, 5, 8, 9, 10, 11, 12, 13, 14 are OK to use.
 */

#include <stdint.h>

#include "dmac_regs.h"
#include "uspace/uspace.h"

struct dmac_handle {
    struct uspace_iop_handle *ioph;
    unsigned chan;
};

/*======================================================================
 * functions (prototype and inline)
 */

int dmac_init(struct dmac_handle *dmah);
int dmac_fini(struct dmac_handle *dmah);

int dmac_alloc_chan(struct dmac_handle *dmah, unsigned chan);
int dmac_free_chan(struct dmac_handle *dmah);

int dmac_start(struct dmac_handle *dmah, uintptr_t cb_busaddr);
int dmac_is_done(struct dmac_handle *dmah);
int dmac_reset(struct dmac_handle *dmah);

uint32_t dmac_read_reg(struct dmac_handle *dmah, unsigned regoff);
uint32_t dmac_read_chan_reg(struct dmac_handle *dmah, unsigned regoff);
void dmac_print(struct dmac_handle *dmah);

#endif /* _dmac_h_ */
