#include "dmac.h"

/*======================================================================
 */

static inline
void dmac_write_reg(struct dmac_handle *dmah, unsigned regoff, uint32_t value)
{
    *(volatile uint32_t *)(dmah->ioph->virt_addr + regoff) = value;
}

uint32_t dmac_read_reg(struct dmac_handle *dmah, unsigned regoff)
{
    return *(volatile uint32_t *)(dmah->ioph->virt_addr + regoff);
}

static inline
void dmac_write_chan_reg(struct dmac_handle *dmah, unsigned regoff, uint32_t value)
{
    dmac_write_reg(dmah, dmah->chan * 0x100 + regoff, value);
}

uint32_t dmac_read_chan_reg(struct dmac_handle *dmah, unsigned regoff)
{
    return dmac_read_reg(dmah, dmah->chan * 0x100 + regoff);
}

/*======================================================================
 */

int dmac_init(struct dmac_handle *dmah)
{
    dmah->ioph = uspace_iop_alloc(DMAC_OFFSET, DMAC_SEGSIZE);
    if(dmah->ioph == 0)
	return -1;

    return 0;
}

int dmac_fini(struct dmac_handle *dmah)
{
    uspace_iop_free(dmah->ioph);
    dmah->ioph = 0;
    return 0;
}

int dmac_alloc_chan(struct dmac_handle *dmah, unsigned chan)
{
    uint32_t v = dmac_read_reg(dmah, DMAC_ENABLE);
    if(v & (1 << chan))
	fprintf(stderr, "Notice: dmac_alloc_chan: chan %d already used\n", chan);

    /* enable DMA globally */
    dmah->chan = chan;
    dmac_write_reg(dmah, DMAC_ENABLE, v | (1 << dmah->chan));
    return 0;
}

int dmac_free_chan(struct dmac_handle *dmah)
{
    uint32_t v = dmac_read_reg(dmah, DMAC_ENABLE);
    if(v & (1 << dmah->chan)) {
	/* disable DMA globally */
	dmac_write_reg(dmah, DMAC_ENABLE, v & ~(1 << dmah->chan));
	dmah->chan = -1;
	return 0;
    } else {
	fprintf(stderr, "dmac_free_chan: chan %d not allocated\n", dmah->chan);
	return -1;
    }
}

int dmac_start(struct dmac_handle *dmah, uintptr_t cb_busaddr)
{
    /* reset pending DMA */
    dmac_write_chan_reg(dmah, DMAC_CS, DMACCS_RESET);

    /* start new DMA */
    dmac_write_chan_reg(dmah, DMAC_CONBLK_AD, cb_busaddr);
    dmac_write_chan_reg(dmah, DMAC_CS, DMACCS_ACTIVE);
    return 0;
}

int dmac_is_done(struct dmac_handle *dmah)
{
    uint32_t v = dmac_read_chan_reg(dmah, DMAC_CS);
    return (v & (DMACCS_END | DMACCS_ACTIVE)) == DMACCS_END;
}

int dmac_reset(struct dmac_handle *dmah)
{
    dmac_write_chan_reg(dmah, DMAC_CS, DMACCS_RESET);
    return 0;
}

void dmac_print(struct dmac_handle *dmah)
{
    printf("DMA chan %d:\n", dmah->chan);
    printf(" CS\t=%08x\n", dmac_read_chan_reg(dmah, DMAC_CS));
    printf(" CB_AD\t=%08x\n", dmac_read_chan_reg(dmah, DMAC_CONBLK_AD));
    printf(" TI\t=%08x\n", dmac_read_chan_reg(dmah, DMAC_TI));
    printf(" SRC_AD\t=%08x\n", dmac_read_chan_reg(dmah, DMAC_SOURCE_AD));
    printf(" DEST_AD=%08x\n", dmac_read_chan_reg(dmah, DMAC_DEST_AD));
    printf(" XLEN\t=%08x\n", dmac_read_chan_reg(dmah, DMAC_TXFR_LEN));
    printf(" DEBUG\t=%08x\n", dmac_read_chan_reg(dmah, DMAC_DEBUG));
    printf("DMAENABLE=0x%08x\n", dmac_read_reg(dmah, DMAC_ENABLE));
}
