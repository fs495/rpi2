/*
 * In-memory graphics library
 *
 * Function signature looks like Adafruit GFX library
 */

#include <stdint.h>

typedef int g_coord;
typedef unsigned g_color;

static inline g_color g_mkcolor(uint8_t r, uint8_t g, uint8_t b) {
    return (r << 16) | (g << 8) | (b);
}

#define G_BLACK		g_mkcolor(0, 0, 0)
#define G_RED		g_mkcolor(255, 0, 0)
#define G_GREEN		g_mkcolor(0, 255, 0)
#define G_BLUE		g_mkcolor(0, 0, 255)
#define G_CYAN		g_mkcolor(0, 255, 255)
#define G_MAGENTA	g_mkcolor(255, 0, 255)
#define G_YELLOW	g_mkcolor(255, 255, 0)
#define G_WHITE		g_mkcolor(255, 255, 255)

void g_setBaseAddr(void *base_addr);

void g_drawPixel(g_coord x, g_coord y, g_color color);
g_color g_getPixel(g_color x, g_color y);

void g_drawLine(g_coord x0, g_coord y0, g_coord x1, g_coord y1, g_color color);
void g_drawFastVLine(g_coord x, g_coord y, g_coord h, g_color color);
void g_drawFastHLine(g_coord x, g_coord y, g_coord w, g_color color);
void g_drawRect(g_coord x, g_coord y, g_coord w, g_coord h, g_color color);
void g_fillRect(g_coord x, g_coord y, g_coord w, g_coord h, g_color color);
void g_fillScreen(g_color color);

void g_drawCircle(g_coord x0, g_coord y0, g_coord r, g_color color);
void g_fillCircle(g_coord x0, g_coord y0, g_coord r, g_color color);
void g_drawRoundRect(g_coord x0, g_coord y0, g_coord w, g_coord h,
		     g_coord radius, g_color color);
void g_fillRoundRect(g_coord x0, g_coord y0, g_coord w, g_coord h,
		     g_coord radius, g_color color);
