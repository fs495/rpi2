% Michener's algorithm
% ref: http://dencha.ojaru.jp/programs_07/pg_graphic_09a1.html

r = 99;

d = 3 - 2 * r;
cy = r;
xlist = [0, 0, +r, -r];
ylist = [+r, -r, 0, 0];

cx = 0;
while cx <= cy
    if(d < 0)
      d += 6 + 4 * cx;
    else
      d += 10 + 4 * cx - 4 * cy;
      cy -= 1;
    end

    xlist = [xlist, +cy, +cx, -cx, -cy, -cy, -cx, +cx, +cy];
    ylist = [ylist, +cx, +cy, +cy, +cx, -cx, -cy, -cy, -cx];
    cx += 1;
end

close all;
scatter(xlist, ylist, 'blue', '.');
