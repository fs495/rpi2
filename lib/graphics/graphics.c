#include "graphics.h"

static uint8_t *base;

enum {
    WIDTH = 128,
    HEIGHT = 128,
    OPP = 3, /* octets per pixel */
    STRIDE = OPP * WIDTH, /* octets per line */
};

void g_setBaseAddr(void *base_addr)
{
    base = base_addr;
}

void g_drawPixel(g_coord x, g_coord y, g_color color)
{
    uint8_t *ptr = base + y * STRIDE + x * OPP;
    *ptr++ = color;
    *ptr++ = color >> 8;
    *ptr   = color >> 16;
}

g_color g_getPixel(g_color x, g_color y)
{
    uint8_t *ptr = base + y * STRIDE + x * OPP;
    return (ptr[2] << 16) | (ptr[1] << 8) | ptr[0];
}

void g_drawLine(g_coord x0, g_coord y0, g_coord x1, g_coord y1, g_color color)
{
    g_coord ax, ay, tx, ty, frac;
    /* ax=|x1-x0|とay=|y1-y0|の大小を比較し、大きいほうの軸でループを回す */
    ax = x1 > x0 ? x1 - x0 : x0 - x1;
    ay = y1 > y0 ? y1 - y0 : y0 - y1;

    if(ax >= ay) {
	/* x0 > x1なら開始/終了を交換 */
	if(x0 > x1) {
	    tx = x1; x1 = x0; x0 = tx; ty = y1; y1 = y0; y0 = ty;
	}
	for(tx = 0, ty = 0, frac = 0; tx < ax; tx++) {
	    g_drawPixel(x0 + tx, y0 + ty, color);
	    frac += ay;
	    if(frac >= ax) {
		ty += (y1 > y0) ? 1 : -1;
		frac -= ax;
	    }
	}
    } else {
	/* y0 > y1なら開始/終了を交換 */
	if(y0 > y1) {
	    ty = y1; y1 = y0; y0 = ty; tx = x1; x1 = x0; x0 = tx;
	}
	for(ty = 0, tx = 0, frac = 0; ty < ay; ty++) {
	    g_drawPixel(x0 + tx, y0 + ty, color);
	    frac += ax;
	    if(frac >= ay) {
		tx += (x1 > x0) ? 1 : -1;
		frac -= ay;
	    }
	}
    }
}

void g_drawFastVLine(g_coord x, g_coord y, g_coord h, g_color color)
{
    uint8_t *ptr = base + y * STRIDE + x * OPP;
    for(int i = 0; i <= h; i++) {
	ptr[0] = color;
	ptr[1] = color >> 8;
	ptr[2] = color >> 16;
	ptr += STRIDE;
    }
}

void g_drawFastHLine(g_coord x, g_coord y, g_coord w, g_color color)
{
    uint8_t *ptr = base + y * STRIDE + x * OPP;
    for(int i = 0; i <= w; i++) {
	*ptr++ = color;
	*ptr++ = color >> 8;
	*ptr++ = color >> 16;
    }
}

void g_drawRect(g_coord x, g_coord y, g_coord w, g_coord h, g_color color)
{
    g_drawFastHLine(x, y,   w, color);
    g_drawFastHLine(x, y+h, w, color);

    g_drawFastVLine(x,   y, h, color);
    g_drawFastVLine(x+w, y, h, color);
}

void g_fillRect(g_coord x, g_coord y, g_coord w, g_coord h, g_color color)
{
    g_coord ty;
    for(ty = 0; ty <= h; ty++)
	g_drawFastHLine(x, x + w, y + ty, color);
}

void g_fillScreen(g_color color)
{
    g_coord i;
    uint8_t *ptr = base;
    for(i = 0; i < WIDTH * HEIGHT; i++) {
	*ptr++ = color;
	*ptr++ = color >> 8;
	*ptr++ = color >> 16;
    }
}

void g_drawCircle(g_coord x0, g_coord y0, g_coord r, g_color color)
{
    /* 参照: http://dencha.ojaru.jp/programs_07/pg_graphic_09a1.html */
    g_coord d = 3 - 2 * r;
    g_coord cx = 0;
    g_coord cy = r;
    g_drawPixel(x0,      y0 + cy, color);
    g_drawPixel(x0,      y0 - cy, color);
    g_drawPixel(x0 + cx, y0,      color);
    g_drawPixel(x0 - cx, y0,      color);

    while(cx <= cy) {
	if(d < 0) {
	    d += 6 + 4 * cx;
	} else {
	    d += 10 + 4 * cx - 4 * cy;
	    cy -= 1;
	}
	g_drawPixel(x0 + cx, y0 + cy, color);
	g_drawPixel(x0 + cx, y0 - cy, color);
	g_drawPixel(x0 - cx, y0 - cy, color);
	g_drawPixel(x0 - cx, y0 + cy, color);
	g_drawPixel(x0 + cy, y0 + cx, color);
	g_drawPixel(x0 + cy, y0 - cx, color);
	g_drawPixel(x0 - cy, y0 - cx, color);
	g_drawPixel(x0 - cy, y0 + cx, color);
	cx++;
    }
}

void g_fillCircle(g_coord x0, g_coord y0, g_coord r, g_color color)
{
    g_coord d = 3 - 2 * r;
    g_coord cx = 0;
    g_coord cy = r;
    g_drawPixel(x0,      y0 + cy, color);
    g_drawPixel(x0,      y0 - cy, color);
    g_drawPixel(x0 + cx, y0,      color);
    g_drawPixel(x0 - cx, y0,      color);

    while(cx <= cy) {
	if(d < 0) {
	    d += 6 + 4 * cx;
	} else {
	    d += 10 + 4 * cx - 4 * cy;
	    cy -= 1;
	}
	g_drawFastHLine(x0 - cx, y0 - cy, 2 * cx, color);
	g_drawFastHLine(x0 - cx, y0 + cy, 2 * cx, color);
	g_drawFastHLine(x0 - cy, y0 - cx, 2 * cy, color);
	g_drawFastHLine(x0 - cy, y0 + cx, 2 * cy, color);
	cx++;
    }
}

void g_drawRoundRect(g_coord x0, g_coord y0, g_coord w, g_coord h,
		     g_coord radius, g_color color)
{
}

void g_fillRoundRect(g_coord x0, g_coord y0, g_coord w, g_coord h,
		     g_coord radius, g_color color)
{
}

