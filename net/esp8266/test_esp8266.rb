# -*- coding: utf-8 -*-
require 'test/unit'
require './esp8266'

class TestCase_ESP8266 < Test::Unit::TestCase
    def setup
	@wifi = ESP8266.new(ESP8266::DEFAULT_DEVNAME)
    end

    def test_init
	puts "### リセットかけながら、応答してこないことを検知できるか確認中…"
	dir='/sys/class/gpio'
	gpio=8
	system("sudo sh -c 'echo #{gpio} > #{dir}/export'")
	system("sudo sh -c 'echo out > #{dir}/gpio#{gpio}/direction'")
	system("sudo sh -c 'echo 0 > #{dir}/gpio#{gpio}/value'")
	assert_raise(ESP8266::Error) {
	    @wifi.init()
	}
	system("sudo sh -c 'echo 1 > #{dir}/gpio#{gpio}/value'")
	system("sudo sh -c 'echo #{gpio} > #{dir}/unexport'")

	puts "### リセットを解除して、応答を検知できるか確認中…"
	assert_nothing_thrown {
	    @wifi.init()
	}
    end

    def test_version()
	puts "### バージョン取得中…"
	pp @wifi.version()
    end

    def test_list_ap()
	puts "### 付近のAPを検索中…"
	pp @wifi.list_ap()
    end

    def test_joined_ap()
	puts "### 接続先APの情報取得中…"
	pp @wifi.joined_ap()
    end
end
