#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require './esp8266'
require './work/secret'

devname = ESP8266::DEFAULT_DEVNAME
wifi = ESP8266.new(devname)
wifi.init()

if false
    wifi.atcmd_test("AT")
    wifi.atcmd_test("AT")
    wifi.atcmd_test("AT+GMR")
    wifi.atcmd_test("AT+CWJAP?", 10000)
end

puts "### Set Wifi mode (AP & station)"
wifi.set_wifi_mode(:ap_station)

if false
    puts "### List of near AP"
    pp wifi.list_ap()
    puts "### Joining to AP..."
    wifi.join_ap(STA_SSID, STA_PASSWD)
    puts "### Joined AP"
    pp wifi.joined_ap()
    puts "### List of AP"
    pp wifi.list_ap()
    #puts "### Disconnecting..."
    #wifi.disconnect()
end

if false
    #puts "### Configuring as AP..."
    #wifi.configure_ap(AP_SSID, AP_PASSWD, 1, :wpa_psk)
    puts "### Current config as AP"
    pp wifi.get_ap_config()
    puts "### List of connecting stations"
    pp wifi.station_list()
end

if true
    puts "### Ping to www.google.com..."
    wifi.ping("www.google.com")
    puts "### Connecting www.google.com..."
    wifi.connect_tcp("www.google.com", 80)
    puts "### Sending data..."
    wifi.send("GET /?q=ESP8266 HTTP/1.0\r\n\r\n")
    puts "### Retriving data..."
    while buf = wifi.recieve()
	#puts buf
    end
    puts "### Closing connection..."
    wifi.close()
end
