#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
# ESP8266にATコマンドを実装したESP-WROOM-02などのモジュール用

require 'serialport'
require 'pp'

class ESP8266 < SerialPort
    DEFAULT_DEVNAME = '/dev/ttyAMA0'
    DEFAULT_BAUD = 115200
    DEFAULT_TIMEOUT = 1000

    # SerialPortを拡張しているので、以下の読み込み動作になる
    # ・readは指定されたバイト数を受信するか、タイムアウトか、EOFでリターン
    # ・getsはタイムアウトするかEOFでリターン
    # ・これらがタイムアウトした場合は空のStringを返す
    # ・EOF時はnilを返す

    def initialize(devname = DEFAULT_DEVNAME, baud = DEFAULT_BAUD)
	super(devname, baud, 8, 1, SerialPort::NONE)
    end

    class Error < StandardError
        # no unique definition
    end

    # ATコマンドを発行する
    def atcmd(cmd, timeout = DEFAULT_TIMEOUT)
	self.read_timeout = timeout
	self.write(cmd + "\r\n")
    end

    # ATコマンドを発行し、その結果でイテレータを呼び出す。
    # イテレータ内でbreakするか、タイムアウトするか、EOFの時に実行を終了する
    def atcmd_loop(cmd, timeout = DEFAULT_TIMEOUT)
	atcmd(cmd, timeout) if cmd != nil

	while true
	    buf = self.gets
	    # タイムアウト時=>'', EOF時=>nil
	    if buf != '' && buf != nil
		yield buf.chomp
	    else
		yield nil
		break
	    end
	end
    end

    # ATコマンドの発行結果をデバッグ表示する
    def atcmd_test(cmd, timeout = DEFAULT_TIMEOUT)
	#Kernel::puts "*** sending: #{cmd}"
	self.atcmd_loop(cmd, timeout) do |buf|
	    case buf
	    when nil
		break
	    when 'OK', 'FAIL'
		pp buf
		break
	    else
		pp buf
	    end
	end
    end

    # 'OK'が返るまでの出力を得て、配列で返す
    def atcmd_generic_loop(cmd, timeout = DEFAULT_TIMEOUT)
	ret = []
	self.atcmd_loop(cmd, timeout) do |buf|
	    case buf
	    when nil
		break # タイムアウトしたら打ち切り
	    when 'OK'
		break
	    else
		ret << buf
	    end
	end
	return ret
    end

    #----------------------------------------------------------------------

    # 応答を受け取れるように初期化する
    def init()
	ready = false
	(0..5).each do
	    # 先行の出力をなるべく捨てる
	    self.read_nonblock(0)

	    # エコーしない設定を試み、OKを待つ
	    self.atcmd_loop("ATE0", DEFAULT_TIMEOUT) do |buf|
		ready = true if buf == 'OK'
	    end
	    break if ready
	end

	raise Error.new("no response") if !ready
    end

    # ファームウェアのバージョンをStringのArrayで返す
    def version()
	return atcmd_generic_loop("AT+GMR")
    end
    
    def restart()
        return atcmd_generic_loop("AT+RST")
    end

    def deep_sleep_mode()
	return atcmd_generic_loop("AT+GSLP")
    end

    def set_sleep_mode(mode)
	return atcmd_generic_loop("AT+SLEEP=#{mode}")
    end

    def get_sleep_mode(mode)
	atcmd_loop("AT+SLEEP?") do |buf|
	    if buf =~ /\+SLEEP:(\d+)/
		return $1.to_i
	    else
		raise Error.new("Unknown resp: #{buf}")
	    end
	end
    end

    def set_radio_power(x)
	return atcmd_generic_loop("AT+RFPOWER=#{x}")
    end

    def set_wifi_mode(mode)
	case mode
	when :station
	    mode = 1
	when :ap
	    mode = 2
	when :ap_station
	    mode = 3
	end
	return atcmd_generic_loop("AT+CWMODE=#{mode}")
    end

    #----------------------------------------------------------------------
    # アクセスポイントに接続する機能

    # APに接続する
    def join_ap(ssid, passwd, bssid = nil)
	arg = "\"#{ssid}\",\"#{passwd}\""
	arg += ",\"#{bssid}\"" if bssid != nil
	atcmd_loop("AT+CWJAP=#{arg}", 15000) do |buf|
	    #pp [:join_ap, buf]
	    case buf
	    when "OK", "ERROR"
		return buf
	    when nil
		raise Error.new("no resp")
	    end
	end
    end

    # 接続中のAPの情報をHashで返す
    def joined_ap()
	ret = nil
	atcmd_loop("AT+CWJAP?") do |buf|
	    #pp [:joined_ap, buf]
	    if buf =~ /\+CWJAP:\"(.*)\",\"(.*)\",(.*),(.*)/
		ret = {:ssid => $1, :mac => $2, :chan => $3.to_i,
		    :intensity => $4.to_i}
	    end
	end
	return ret
    end

    # 検出したAPのリストを返す
    def list_ap()
        ret = []
	atcmd_loop("AT+CWLAP", 10000) do |buf|
	    #pp [:list_ap, buf]
	    if buf =~ /\+CWLAP:\((.*),\"(.*)\",(.*),\"(.*)\",(.*)\)/
		ret << {:ecn => $1.to_i, :ssid => $2, :intensity => $3,
		    :mac => $4, :chan => $5.to_i}
	    elsif buf == "OK"
		break
            elsif buf == ''
		# ignore
	    else
		raise Error.new("Unknown resp: #{buf}")
	    end
	end
	return ret
    end

    # APから切断する
    def disconnect()
	return atcmd_generic_loop("AT+CWQAP")
    end

    #----------------------------------------------------------------------
    # アクセスポイントとしての機能

    def configure_ap(ssid, passwd, chan, ecn)
	hash = { :open => 0, :wpa_psk => 2, :wpa2_psk => 3, :wpa_wpa2_psk => 4}
	ecn = hash[ecn] if hash[ecn] != nil
	arg = "\"#{ssid}\",\"#{passwd}\",#{chan},#{ecn}"
	return atcmd_generic_loop("AT+CWSAP=#{arg}", 10000)
    end

    def get_ap_config()
	atcmd_loop("AT+CWSAP?", 10000) do |buf|
	    if buf =~ /\+CWSAP:\"(.*)\",\"(.*)\",(.*),(.*)/
		return {:ssid => $1, :mac => $2,
		    :chan => $3.to_i, :ecn => $4.to_i}
	    end
	    raise Error.new("no resp")
	end
    end

    def station_list()
	ret = []
	atcmd_loop("AT+CWLIF") do |buf|
	    if buf =~ /(.*),(.*)/
		ret << {:ip => $1, :mac => $2}
	    end
	end
	return ret
    end

    #----------------------------------------------------------------------
    # IP通信の機能

    def ping(addr)
	result = nil
	atcmd_loop("AT+PING=\"#{addr}\"") do |buf|
	    #pp [:ping, buf]
	    if buf =~ /\+(\d+)/
		result = $1.to_i
	    elsif buf == "OK" || buf == "ERROR" || buf == nil
		break
	    end
	end
	return result
    end

    def connect_tcp(addr, port, keepalive=nil)
	arg = "\"TCP\",\"#{addr}\",#{port}"
	arg += ",#{keepalive}" if keepalive != nil
	connect(arg)
    end

    def connect_udp(addr, port, localport=nil, udpmode=nil)
	arg = "\"UDP\",\"#{addr}\",#{port}"
	if localport != nil
	    arg += ",#{localport}"
	    if udpmode != nil
		arg += ",#{udpmode}"
	    end
	end
	connect(arg)
    end

    def set_multiplex(mode)
	atcmd_loop("AT+CIPMUX=#{mode}") do |buf|
	    #pp [:connect1, buf]
	    if buf == "OK"
		break
	    elsif buf != ""
		raise Error.new
	    end
	end
    end

    def set_data_info(mode)
	atcmd_loop("AT+CIPDINFO=#{mode}") do |buf|
	    #pp [:connect2, buf]
	    if buf == "OK"
		break
	    elsif buf != ""
		raise Error.new
	    end
	end
    end

    def connect(arg)
	set_multiplex(0)
	set_data_info(1)
	atcmd_loop("AT+CIPSTART=#{arg}", 10000) do |buf|
	    pp [:connect3, buf]
	    case buf
	    when "OK"
		break
	    when "CONNECT", ""
		@connected = true
	    else
		raise Error.new
	    end
	end
    end

    def send(data)
	data = data.to_s
	atcmd_loop("AT+CIPSEND=#{data.length}") do |buf|
	    pp [:send1, buf]
	    if buf == "> "
		break
	    elsif buf != "" && buf != "OK"
		raise Error.new
	    end
	end

	write(data)

	atcmd_loop(nil) do |buf|
	    pp [:send2, buf]
	    if buf == "SEND OK"
		break
	    elsif buf == "" || buf =~ /Recv \d+ bytes/
		# do nothing
	    else
		raise Error.new("transmission failed: #{buf}")
	    end
	end
    end

    def recieve(timeout=1000)
	return nil if !@connected

	self.read_timeout = timeout
	ret = ''
	while true
	    buf = self.gets
	    if buf =~ /^\+IPD,(\d+),(.*),(.*):(.*)/
		len, rip, rport, data = $1.to_i, $2, $3.to_i, $4 + "\n"
		pp [:recv1, len, rip, rport, data.length, data]

		ret += data
		len -= data.length

		while len > 0
		    pp [:recv3, len]
		    data = self.read(len)
		    break if data == nil # タイムアウト
		    pp [:recv2, len, data.length, data]
		    ret += data
		    len -= data.length
		end
		break
	    elsif buf == "CLOSED\r\n"
		@connected = false
		return ret
	    elsif buf == nil
		return ret
	    elsif buf == "\r\n"
		# do nothing
	    else
		Kernel::puts "Warning: unknown resp: #{buf}"
	    end
	end
	return ret
    end

    def close()
	return if !@connected
	
	atcmd_loop("AT+CIPCLOSE") do |buf|
	    case buf
	    when "OK"
		@connected = false
		break
	    when ""
		# do nothing
	    else
		raise Error.new("close failed: #{buf}")
	    end
	end
    end
end
