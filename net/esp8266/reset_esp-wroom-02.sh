#!/bin/sh

set -e

# ESP-WROOM-02へのRST信号でリセットをかける例
# RaspberryのGPIO8に接続されているので、一定時間Lレベルにする

GPIO=8

cd /sys/class/gpio
echo $GPIO > export
echo out > gpio$GPIO/direction

echo 0 > gpio$GPIO/value
sleep 0.1
echo 1 > gpio$GPIO/value

echo $GPIO > unexport
