Raspberry Pi用プロジェクト

----------------------------------------------------------------------
ライブラリ(lib/*)

uspace
	/dev/memをmmapすることにより物理アドレス空間のI/Oやメモリに
	アクセスする低レベルライブラリ

gpio
	GPIO操作
	(低レベルレイヤーとしてsysfsまたはuspaceを使用)
	gpio_sysfs.hのインターフェースだけ、ハンドル用の構造体ではなく、
	ファイルディスクリプタ単体で管理する。

dmac
	DMAコントローラ操作
	(低レベルレイヤーとしてmmapを使用)

spi
	SPI操作
	(低レベルレイヤーとしてspidevまたはmmapを使用)

dl144128tf
	128x128カラー液晶ライブラリ

graphics
	オフスクリーンバッファ上のグラフィクスライブラリ
	現時点での実装はdl144128tf用にのみ対応

----------------------------------------------------------------------
仮想アドレス空間・物理アドレス空間・バスアドレス空間

https://github.com/figue/raspberry-pi-kernel/blob/master/Documentation/bus-virt-phys-mapping.txt

仮想アドレス: CPU translated address
物理アドレス: CPU untranslated
バスアドレス: the address of memory as seen by OTHER devices, not the CPU

/dev/memをmmapするときは物理アドレス
DMAコントローラが扱うのはバスアドレス

----------------------------------------------------------------------
IO領域

https://www.raspberrypi.org/forums/viewtopic.php?t=63778&p=472141

I/O Peripheral starts
at 0xF200_0000 at ARM Virtual address.
at 0x2000_0000 or 0x3f00_0000 at real physical address
at 0x7E00_0000 at CPU Bus address

"BCM2835-ARM-Peripherals.pdf"にあるレジスタのアドレスはバスアドレス。

----------------------------------------------------------------------
メモリ領域

物理アドレス 0からメモリは始まり、
バスアドレス 0xc000_0000にマップされる('C'エイリアス:キャッシュされない)

----------------------------------------------------------------------
uspaceライブラリ

void *uspace_map(size_t phys_addr, size_t size)
	物理アドレスとサイズを指定して仮想アドレス空間にマップ

struct uspace_iop_handle *uspace_iop_alloc(size_t iop_offset, size_t size)
	IO領域からのオフセットとサイズを指定して、
	仮想アドレス空間にマップ(uspace_mapを使う)

struct uspace_mem_handle *uspace_mem_alloc(size_t size)
	バスアドレスの分かるメモリ領域を確保(mailbox APIを使う)して、
	仮想アドレス空間にマップする(uspace_mapを使う)

----------------------------------------------------------------------
dmacライブラリ

<使い方>
dmac_init()でDMAハンドルを初期化

dmac_alloc_chan()で使用するDMAチャンネルを宣言/割り当てる

uspace_mem_alloc()でバスアドレスの分かるメモリ領域を割り当てる。

割り当て対象:
- DMAのコントロールブロック(struct dmac_ctrlblk)
- DMAの転送元 or 転送先

コントロールブロックをセットアップ
- ti, txfr_len
- source_ad, dest_ad: メモリの場合はバスアドレス

dmac_start()を呼び出し

dmac_is_done() == 0の間ウェイトする

dmac_reset()を呼び出し、DMAを停止させる
